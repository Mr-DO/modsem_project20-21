import pylode

html = pylode.MakeDocco(
    input_data_file="modsem_project20-21.ttl",
    outputformat="html"
).document()

f = open("documentation.html", "w")
f.write(html)
f.close()