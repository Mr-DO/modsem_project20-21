<?php
    $target_dir = "../image/profileImg/";
    $old_target_file = $target_dir . $global_username;
    $profileImage = checkUserProfile($old_target_file) !== null ? checkUserProfile($old_target_file) : '../image/icons/profile-image-1.png';
?>

<div class="div-left-menu">
    <div id="element-display-menu">
        <img style="display: block;" id="hamburger_open" alt="menu" onclick="hamburgerGestion(this)" src="../image/icons/hamburger-icon.png" />
        <img style="display: none;" id="hamburger_close" alt="menu" onclick="hamburgerGestion(this)" src="../image/icons/hamburger-icon-close.png" />
    </div>
    <div id="elements-left-menu">
        <a href="index.php?subvar=update-img-profile" id="image-user" class="element-left-menu">        
            <img style="width: 100px; height: 80px; margin-bottom: 5px;" alt="image user" src="<?php echo $profileImage; ?>">
        </a>
        <a href="index.php?subvar=reservations" id="my-reservations" class="element-left-menu"> 
            <img alt="immagine lista delle prenotazioni" src="../image/icons/booking-icon.png">   
            <div style="padding-top: 10px;"> Prenotazioni </div>
        </a>
        <a href="index.php?subvar=do-reservations" id="do-reservations" class="element-left-menu"> 
            <img style="width: 55px; height: 40px;" alt="immagine prenotare" src="../image/icons/prenotare.jpg">
            <div style="padding-top: 10px;"> Prenotare </div> 
        </a>
    </div>
</div>