<?php

?>

<div class="paginate" id="paginate">
    <a href="<?php echo $url . "&page=1" ?>" onclick="<?php if($page <= 1) echo 'return false'; ?>" style="text-decoration: none;"> 
        <input type="button" id="first" value="first" /> 
    </a>
    <a href="<?php echo $url . "&page=" . ($page-1) ?>" onclick="<?php if($page <= 1) echo 'return false'; ?>" style="text-decoration: none;"> 
        <input type="button" id="previous" value="previous" /> 
    </a>
    <a href="<?php echo $url . "&page=" . ($page+1) ?>" onclick="<?php if($page >= $endPage) echo 'return false'; ?>" style="text-decoration: none;"> 
        <input type="button" id="next" value="next" /> 
    </a>
    <a href="<?php echo $url . "&page=" . $endPage ?>" onclick="<?php if($page >= $endPage) echo 'return false'; ?>"  style="text-decoration: none;"> 
        <input type="button" id="last" value="last" /> 
    </a>
</div>