<?php

?>

<div class="about-page">
    <h2> Progetto di Modellazione Concettuale per il Web Semantico(ModSem20 2020-2021), parte 2 estensioni(Progetti singoli)</h2>
    <p> 
        <span style="color:red; text-decoration: underline; font-size: 2em"> Consegna </span> <br />
        Realizzare un Applicazione client. <br />
    </p>
    <p>
        <img style="float:left; width: 300px; height: 250px; margin-right: 20px" alt="bookink image" src="image/icons/realizzazione-di-un-sito-web.png"> <br />
        <ul>
            <li>Utilizzando una Linked Data Platform (Virtuoso, GraphDB, BlazeGrapho altri–incluse librerie per data storage e manipulation come Jena) e 
                il suo SPARQL endpoint (o API fornita dalla LDP), creare l'applicazione client che interroga l'ontologiasecondo lo schema definito in precedenza(via SPARQL o API). 
                Se la LDP lo supporta, è possibile creare una o più query per l’inserimento di dati. </li>
            <li>Per i gruppi di due persone, includerel'interrogazione dialtre sorgenti di dati. </li>
            <li> Note:
                <ul>
                    <li>La grafica dell’interfaccia non è rilevante</li>
                    <li>Il codice deve essere commentato.</li>
                    <li>Preparare una documentazione sintetica del progetto(1/2 pagina)</li>
                </ul>
            </li>
        </ul>
    </p>
    <p style="clear:both;">
        Linguaggi di programmazione : PHP, JAVASCRIPT. <br />
        LAYOUT E STYLE : HTML e CSS. 
    </p>
</div>