<?php

?>

<div class="referimenti-page">
    <h2> Per la realizzazione il nostro progeto in generale e la nostra applicazione client in particolare, abbiamo consultato varie siti web e documenti online.</h2>
    <p>
        Ecco un elenco di alcuni dei tanti siti e documenti online che abbiamo visitato durante la realizzazione del sito. <br /> <br />
    </p>
    <p>
        <ul>
            <li> <a href="https://it.wikipedia.org/wiki/Immigrazione#Bibliografia" target="_blank">https://it.wikipedia.org/wiki/Immigrazione#Bibliografia"</a> </li>
            <li> <a href="https://www.explodingafrica.com/2018/07/25/quali-migranti-le-tipologie-migratorie" target="_blank">https://www.explodingafrica.com/2018/07/25/quali-migranti-le-tipologie-migratorie</a> </li>
            <li> <a href="https://www.unisa.it/centri\_e\_vari/ops/areetematiche/immigrazione" target="_blank">https://www.unisa.it/centri\_e\_vari/ops/areetematiche/immigrazione</a> </li>
            <li> <a href="https://www.unisa.it/uploads/4323/a._statistiche_sulla_migrazione_nel_mondo,_in_europa_e_in_italia.pdf" target="_blank">https://www.unisa.it/centri\_e\_vari/ops/areetematiche/immigrazione</a> </li>
            <li> <a href="https://it.wikipedia.org/wiki/Emigrazione_italiana" target="_blank">https://it.wikipedia.org/wiki/Emigrazione_italiana</a> </li>
            <li> <a href="https://gitlab.com/Mr-DO/modsem_project20-21" target="_blank">https://gitlab.com/Mr-DO/modsem_project20-21</a> </li>
        </ul>
    </p>
    <p style="color: red;"> Non si finisce mai di imparare.</p>
</div>