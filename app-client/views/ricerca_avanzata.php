<form style="margin-top: 50px" id="form-imm-show-all" method="get" action="?">
    <p>
        <label for="inputName-form-imm-show-all">Nome : </label>
        <input size="50%" type="text" id="inputName-form-imm-show-all" /> 
    </p>
    <p>
        <label for="inputCognome-form-imm-show-all">Cognome : </label>
        <input size="50%" type="text" id="inputCognome-form-imm-show-all" />
    </p>
    <p> 
        <input type="radio" id="nazione_choose_origine_p" name="choose_origine_p" value="nazione_origine" checked="checked" /> 
        <label for="nazione_choose_origine_p">Per Nazione di Origine</label>
        <input type="radio" id="continente_choose_origine_p" name="choose_origine_p" value="continente_origine" />
        <label for="continente_choose_origine_p">Per Continente di Origine</label>
    </p>
    <p id="p_nazione_origine">
        <label for="nazione_origine">Schegliere NAzione di Origine : </label>
        <select id="nazione_origine">
        </select>
    </p>
    <p id="p_continente_origine">
        <label for="continente_origine">Schegliere Continente di Origine : </label>
        <select id="continente_origine">
        </select>
    </p>
    <p> 
        <input type="radio" id="nazione_choose_residenza_p" name="choose_residenza_p" value="nazione_residenza" checked="checked" /> 
        <label for="nazione_choose_residenza_p">Per Nazione di Residenza</label>
        <input type="radio" id="continente_choose_residenza_p" name="choose_residenza_p" value="continente_residenza" />
        <label for="continente_choose_residenza_p">Per Continente di Residenza</label>
    </p>
    <p id="p_nazione_residenza">
        <label for="nazione_residenza">Schegliere NAzione di Residenza : </label>
        <select id="nazione_residenza">
        </select>
    </p>
    <p id="p_continente_residenza">
        <label for="continente_residenza">Schegliere Continente di Residenza : </label>
        <select id="continente_residenza">
        </select>
    </p>
    <p id="p_motivo_immigrazione">
        <label for="motivo_immigrazione">Schegliere Motivo di Immigrazione : </label>
        <select id="motivo_immigrazione">
        </select>
    </p>
    <p>
        <label for="inputanno-form-imm-show-all">Anno Immigrazione : </label>
        <input size="50%" type="text" id="inputanno-form-imm-show-all" />
    </p>
    <p>
        <input style="margin-left: 200px" type="button" id="inputSubmit-form-imm-show-all" value="Invia Richiesta" />
    </p>
</form>

<p id="alert-results-imm-show-all"> </p>
<div id="results-imm-show-all">
</div>

<script>
    var url = "http://localhost:8890/sparql/"
    var query_nazione = "SELECT DISTINCT ?nazione WHERE { ?nazione imm:situatoInContinente ?c.}";
    var key = "dba";
    var query_url_nazione = encodeURI(url+"?default-graph-uri="+"&query="+query_nazione+"&wskey="+key+"&format=json&timeout=0");

    var continent = ["", "Africa" , "America" , "Asia" , "Europa" , "Oceania"]
    var motivi = [ "", 
        "MotivoClimatica", 
        "MotivoCriminale", 
        "MotivoDiDisastroNaturale", 
        "MotivoDiIstruzione", 
        "MotivoDiLavoro", 
        "MotivoFamilliare", 
        "MotivoPersonale", 
        "MotivoPolitico", 
        "MotivoReligioso", 
        "MotivoSanitaria", 
        "RicercaDiVitaMigliore"
    ]
            
    $(document).ready(function() 
    {
        for(var i=0; i<continent.length; i++)
        {
            $('#continente_origine').append($("<option></option>").attr("value", continent[i]).text(continent[i])); 
            $('#continente_residenza').append($("<option></option>").attr("value", continent[i]).text(continent[i]));
        }
        for(var i=0; i<motivi.length; i++)
        {
            $('#motivo_immigrazione').append($("<option></option>").attr("value", motivi[i]).text(motivi[i])); 
        }
        $.ajax
        ({
            dataType: "jsonp",
            url: query_url_nazione,
            success: function(data)
            {
                var results = Object.values(data.results.bindings);
                var itemscontry = [""];
                for(var i=0; i < results.length; i++)
                {
                    itemscontry.push(Object.values(results[i])[0].value.split("#").pop());  
                }
                $.each(itemscontry, function(key, value) 
                {   
                    $('#nazione_origine').append($("<option></option>").attr("value", value).text(value)); 
                    $('#nazione_residenza').append($("<option></option>").attr("value", value).text(value));
                });
            },
            error: function (xhr, ajaxOptions, thrownError){console.log(xhr);}
        })

        $('input:radio[name="choose_origine_p"]').click(function(event)
        {
            //event.preventDefault();
            if(this.checked && this.value == 'nazione_origine')
            {
                hideElement('#p_continente_origine');
                showElement('#p_nazione_origine');
            }else if(this.checked && this.value != 'nazione_origine')
            {
                showElement('#p_continente_origine');
                hideElement('#p_nazione_origine');
            }
        });

        $('input:radio[name="choose_residenza_p"]').click(function(event)
        {
            //event.preventDefault();
            if(this.checked && this.value == 'nazione_residenza')
            {
                hideElement('#p_continente_residenza');
                showElement('#p_nazione_residenza');
            }else if(this.checked && this.value != 'nazione_residenza')
            {
                showElement('#p_continente_residenza');
                hideElement('#p_nazione_residenza');
            }
        });

        $("#inputSubmit-form-imm-show-all").click(function(e) 
        {
            e.preventDefault();
            var name = document.getElementById("inputName-form-imm-show-all").value;
            var lastName = document.getElementById("inputCognome-form-imm-show-all").value;
            var nazioneO = document.getElementById("nazione_origine").value;
            var nazioneR = document.getElementById("nazione_residenza").value;
            var continenteO = document.getElementById("continente_origine").value;
            var continenteR = document.getElementById("continente_residenza").value;
            var anno = document.getElementById("inputanno-form-imm-show-all").value;
            var motivazione = document.getElementById("motivo_immigrazione").value;
            query = makeQuery(name, lastName, nazioneO, nazioneR, continenteO, continenteR, anno, motivazione);
            console.log(query);
            query_url = encodeURI(url+"?default-graph-uri="+"&query="+query+"&wskey="+key+"&format=json&timeout=0");
            $.ajax
            ({
                dataType: "jsonp",
                url: query_url,
                success: function(data)
                {
                    var results = Object.values(data.results.bindings);
                    alertNoResulta("alert-results-imm-show-all", results.length)
                    addResults("results-imm-show-all", results);
                },
                error: function (xhr, ajaxOptions, thrownError)
                {
                    console.log(xhr);
                }
            })
        });
    });
</script>