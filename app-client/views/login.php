<?php

    require_once '../controllers/session.php';

    $errorDisplay = "none";

    if(isset($_GET['username']) and !empty($_GET['username'])){
        $username = $_GET['username'];
    }else{
        $username = "";
    }
    if(!empty($_SESSION["error"]["username"])) $username = $_SESSION["error"]["username"];
    if(!empty($_SESSION["error"]["message"])) {
        $error_message = $_SESSION["error"]["message"];
        $errorDisplay = "";
    }

    if(isset($_GET['var']) and !empty($_GET['var'] and ($_GET['var']==="min"))) {
        $action = "../controllers/auth-admin.php";
        $_SESSION['modLogin'] = "min";

    }/*else if(isset($_SESSION['modLogin']) and !empty($_SESSION['modLogin'])){
        if($_SESSION['modLogin']==="min") $action = "../controllers/auth-admin.php";
        else $action = "../controllers/auth-users.php";
    }*/
    else {
        $action = "../controllers/auth-users.php";
    }
     unset($_SESSION['error']);
?>
<!DOCTYPE html>
<html>	
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>Home</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" type="text/css" media="screen" href="../css/main.css" />
		<script type="text/javascript" src="../js/main.js"></script>
	</head>
	<body>
        <main class="single-view-body">
            <div class="login-view">
                <div style="margin-bottom: 5px">
                    <span class="slogan"><b><br />Ontolgy project <br /> Domain: Immigration</b></span> 
                </div>
                <div style="background-color: forestgreen;">
                    <?php require_once("../components/login.php"); ?>
                </div>
            </div>
        </main>
    </body>
</html>
<script type="text/javascript" src="../js/ajax.js"></script>