<form style="margin-top: 50px" id="form-imm-show-all" method="get" action="?">
    <p>
        <label for="inputName-form-imm-show-all">Nome : </label>
        <input size="50%" type="text" id="inputName-form-imm-show-all" required/> 
    </p>
    <p>
        <label for="inputCognome-form-imm-show-all">Cognome : </label>
        <input size="50%" type="text" id="inputCognome-form-imm-show-all" />
    </p>
    <p id="p_nazione_origine">
        <label for="nazione_origine">Schegliere NAzione di Origine : </label>
        <select id="nazione_origine" required>
        </select>
    </p>
    <p id="p_nazione_residenza">
        <label for="nazione_residenza">Schegliere NAzione di Residenza : </label>
        <select id="nazione_residenza" required>
        </select>
    </p>
    <p id="p_motivo_immigrazione">
        <label for="motivo_immigrazione">Schegliere Motivo di Immigrazione : </label>
        <select id="motivo_immigrazione" required>
        </select>
    </p>
    <p>
        <label for="inputanno-form-imm-show-all">Anno Immigrazione : </label>
        <input size="50%" type="text" id="inputanno-form-imm-show-all" />
    </p>
    <p>
        <input style="margin-left: 200px" type="button" id="inputSubmit-form-imm-show-all" value="Invia Richiesta" />
    </p>
</form>

<p id="alert-results-imm-show-all"> </p>
<div id="results-imm-show-all">
</div>

<script>
    var url = "http://localhost:8890/sparql/"
    var query_nazione = "SELECT DISTINCT ?nazione WHERE { ?nazione imm:situatoInContinente ?c.}";
    var key = "dba";
    var query_url_nazione = encodeURI(url+"?default-graph-uri="+"&query="+query_nazione+"&wskey="+key+"&format=json&timeout=0");

    var motivi = [
        "MotivoClimatica", 
        "MotivoCriminale", 
        "MotivoDiDisastroNaturale", 
        "MotivoDiIstruzione", 
        "MotivoDiLavoro", 
        "MotivoFamilliare", 
        "MotivoPersonale", 
        "MotivoPolitico", 
        "MotivoReligioso", 
        "MotivoSanitaria", 
        "RicercaDiVitaMigliore"
    ]
            
    $(document).ready(function() 
    {
        for(var i=0; i<motivi.length; i++)
        {
            $('#motivo_immigrazione').append($("<option></option>").attr("value", motivi[i]).text(motivi[i])); 
        }
        $.ajax
        ({
            dataType: "jsonp",
            url: query_url_nazione,
            success: function(data)
            {
                var results = Object.values(data.results.bindings);
                var itemscontry = [];
                for(var i=0; i < results.length; i++)
                {
                    itemscontry.push(Object.values(results[i])[0].value.split("#").pop());  
                }
                $.each(itemscontry, function(key, value) 
                {   
                    $('#nazione_origine').append($("<option></option>").attr("value", value).text(value)); 
                    $('#nazione_residenza').append($("<option></option>").attr("value", value).text(value));
                });
            },
            error: function (xhr, ajaxOptions, thrownError){console.log(xhr);}
        })


        $("#inputSubmit-form-imm-show-all").click(function(e) 
        {
            e.preventDefault();
            var name = document.getElementById("inputName-form-imm-show-all").value;
            var lastName = document.getElementById("inputCognome-form-imm-show-all").value;
            var nazioneO = document.getElementById("nazione_origine").value;
            var nazioneR = document.getElementById("nazione_residenza").value;
            var anno = document.getElementById("inputanno-form-imm-show-all").value;
            var motivazione = document.getElementById("motivo_immigrazione").value;
            if(name.trim() === "" && lastName.trim() === "")
            {
                console.log("query mal formed!");
            }
            else
            {
                query = makeQueryInsert(name, lastName, nazioneO, nazioneR, anno, motivazione);
                console.log(query);
                query_url = encodeURI(url+"?default-graph-uri="+"&query="+query+"&wskey="+key+"&format=json&timeout=0");
            }
            /*$.ajax
            ({
                dataType: "jsonp",
                url: query_url,
                success: function(data)
                {
                    var results = Object.values(data.results.bindings);
                    alertNoResulta("alert-results-imm-show-all", results.length)
                    addResults("results-imm-show-all", results);
                },
                error: function (xhr, ajaxOptions, thrownError)
                {
                    console.log(xhr);
                }
            });*/
        });
    });
</script>