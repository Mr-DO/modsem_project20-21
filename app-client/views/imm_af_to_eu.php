<p id="alert-results-imm-show-all"> </p>
<div id="results-imm-show-all">
</div>

<script>
    var url = "http://localhost:8890/sparql/"
    var query = "SELECT DISTINCT ?nome ?cognome ?nazioneO ?nazioneR ?continenteO ?continenteR ?anno ?motivazione WHERE { "
        + "  ?subject rdf:type ?Immigrato"
        + "; imm:haNazioneDiOrigine ?nazioneO"
        + "; imm:haNazioneDiResidenza ?nazioneR"
        + ". ?nazioneO imm:situatoInContinente ?continenteO"
        + ". ?nazioneR imm:situatoInContinente ?continenteR"
        + ". ?subject imm:haImmigratoPer ?motivazione"
        + ". OPTIONAL { ?subject imm:haNome ?nome.}"
        + ". OPTIONAL { ?subject imm:haCognome ?cognome.}"
        + ". OPTIONAL { ?subject imm:haImmigratoInAnno ?anno.}"
        + ". FILTER (?continenteO = imm:Africa and ?continenteR = imm:Europa)" +
    "}";
    var key = "dba";
    var query_url = encodeURI(url+"?default-graph-uri="+"&query="+query+"&wskey="+key+"&format=json&timeout=0");
            
    $(document).ready(function() 
    {
        $.ajax
        ({
            dataType: "jsonp",
            url: query_url,
            success: function(data)
            {
                var results = Object.values(data.results.bindings);
                alertNoResulta("alert-results-imm-show-all", results.length)
                addResults("results-imm-show-all", results);
            },
            error: function (xhr, ajaxOptions, thrownError)
                {
                    console.log(xhr);
            }
        })
    });
</script>