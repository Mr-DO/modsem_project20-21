<?php

?>

<div class="base-page">
    <h2> Benvenuto nella tua area personale Admin.</h2>
    <p>
        Sei un amministratore quindi leggiti bene i seguenti informazioni <br />
        Gli amministratori sono dei utenti particolari, che hanno dei privilegi, possono :
	 	<ul>
	 		<li> Modificare il knowledge graph, aggiungendo delle triple o rimuovendole tramite questa interfaccia.</li>
	 		<li> Visulizzare alcuni informazzioni, nascosti all'utente comune. </li>
	 	</ul>
 		<p>Dopo ogni modifiche effettuate, usare la ricerca avanzzata per controllare che la modifica è andata a buon fine.</p>
    <p style="clear:both;"> Sappiamo che possiamo contarci su di te!</p>
</div>