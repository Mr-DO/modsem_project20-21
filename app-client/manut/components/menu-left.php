<div class="div-left-menu">
    <div id="element-display-menu">
        <img style="display: block;" id="hamburger_open" alt="menu" onclick="hamburgerGestion(this)" src="../image/icons/hamburger-icon.png" />
        <img style="display: none;" id="hamburger_close" alt="menu" onclick="hamburgerGestion(this)" src="../image/icons/hamburger-icon-close.png" />
    </div>
    <div id="elements-left-menu">
        <a href="#" id="image-user" class="element-left-menu">        
            <img style="width: 60%; height: 80px; margin-bottom: 10px;" alt="image user" src="../image/icons/profile-image-2.jpeg">
            <!-- <div style="padding-top: 10px;"> Cambia Profile </div> -->
        </a>
        <a href="index.php?subvar=immInsert" id="add-teachers" class="element-left-menu"> 
            <img style="width: 50px; height: 50px;" alt="image add immigrant person" src="../image/icons/add-teachers.png">
            <div style="padding-top: 10px;"> Aggiungi Persona Immigrato </div> 
        </a>
        <ul style="list-style: none; margin-left: 0px;">
            <li><p><a href="index.php?subvar=immAfToEu">Immigrati in Europa in provenianza dell'Africa.</a></p></li>
            <li><p><a href="index.php?subvar=immEuToAf">Emmigrati dall'Europa in Africa .</a></p></li>
            <li><p><a href="index.php?subvar=immAllToIt">Immigrati in Italia.</a></p></li>
            <li><p><a href="index.php?subvar=immItToAll">Emmigrati dall'Italia.</a></p></li>
            <li><p><a href="index.php?subvar=immItToUS">Emmigrati dall'Italia verso le Stati Uniti.</a></p></li>
        </ul>
    </div>
</div>