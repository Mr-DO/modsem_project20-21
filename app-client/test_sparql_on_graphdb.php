<!DOCTYPE html>
    <html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>test sparql</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    </head>
    <body>
        <input id="test_jq_sparql" type="button" value="Click Me" />
        <script>
            var url = "http://localhost:8890/sparql/"
            var query = "SELECT DISTINCT ?nome ?cognome ?nazioneO ?nazioneR ?anno ?motivazione WHERE { ?subject rdf:type ?Immigrato; imm:haImmigratoPer ?motivazione; imm:haNazioneDiOrigine ?nazioneO; imm:haNazioneDiResidenza ?nazioneR. ?nazioneO imm:situatoInContinente imm:Africa. ?nazioneR imm:situatoInContinente imm:Europa. OPTIONAL { ?subject imm:haNome ?nome.}. OPTIONAL { ?subject imm:haCognome ?cognome.}. OPTIONAL { ?subject imm:haImmigratoInAnno ?anno.}.}";
            var key = "dba";
            var query_url = encodeURI(url+"?default-graph-uri="+"&query="+query+"&wskey="+key+"&format=json&timeout=0");
            
        $(document).ready(function() 
        {
            $("#test_jq_sparql").click(function(e) 
            {
                e.preventDefault();
                $.ajax
                ({
                    dataType: "jsonp",
                    url: query_url,
                    success: function(data)
                    {
                        console.log("OK");
                        console.log(data.results.bindings[0].cognome.value);
                    },
                    error: function (xhr, ajaxOptions, thrownError)
                    {
                        console.log("Error");
                        console.log(xhr);
                    }
                })
            });
        });
        </script>
    </body>
</html>
