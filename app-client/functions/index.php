<?php
    /**
     *  generate random number wuth n digits
     *  @parameters $length
     *  @return String 
     */

    function getRandomDigits($length){
        $random_number=''; // set up a blank string
        $count=0;
        if($length <= 0) return '0';
        while ($count < $length) {
            $random_digit = mt_rand(0, 9);
            $random_number .= $random_digit;
            $count++;
        }
        return $random_number;
    }

    function getnewMatricola($pre, $digitsLength, $dataBaseHelper, $tablename){
        $matricola = '';
        do{
            $matricola = $pre . getRandomDigits($digitsLength);
        }while(count($dataBaseHelper->checkColum($tablename, 'matricola', $matricola)) !== 0);
        return $matricola;
    }

    function getnewCodice($pre, $digitsLength, $dataBaseHelper, $tablename){
        $codice = '';
        do{
            $codice = $pre . getRandomDigits($digitsLength);
        }while(count($dataBaseHelper->checkColum($tablename, 'codice', $codice)) !== 0);
        return $codice;
    }

    /**
     * cancel some elemts inside of the session
     * @parameter $array
     * @parameter $string
     * @return void 
     */
    function clearSessionPaginate($index){
        if(isset($_SESSION['do-paginate']) and !empty($_SESSION['do-paginate'])){
            foreach($_SESSION['do-paginate'] as $key => $element){
                if($key !== $index) unset($_SESSION['do-paginate'][$key]);
            }
        }
    }

    /**
     * function for get name of file
     * @parameter $String
     * @return String
     */
    function checkUserProfile($name_file){
        $name = [
            $name_file . '.jpg' ,
            $name_file . '.jpeg' ,
            $name_file . '.png' ,
            $name_file . '.gif'
        ];
        foreach($name as $value){
            if(file_exists($value)) return $value;
        }
        return null;
    }
?>