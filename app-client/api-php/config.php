<?php
	/* SELECT NUMBER DATABASE*/
	$PDO_CONFIGURE = 1;
	$MYSQL_CONFIGURE = 0;

	$dir_DAO =  explode("tweb-project", getcwd(), 2)[0];
	includDAO($dir_DAO."tweb-project\DAO");
	includModelTable($dir_DAO."tweb-project\model\\table");



	function includDAO($dir){
		if ($handle = opendir($dir)) {
		    while (($fileName = readdir($handle)) !== false) {
		        if($fileName !== "." and $fileName !== ".." and strrpos($fileName, ".php")){
		        	require_once $dir."\\".$fileName;
		        }
		    }
    		closedir($handle);
		}
	}
	
	function includModelTable($dir){
		if ($handle = opendir($dir)) {
		    while (($fileName = readdir($handle)) !== false) {
		        if($fileName !== "." and $fileName !== ".." and strrpos($fileName, ".php")){
		        	require_once $dir."\\".$fileName;
					//echo $dir."\\".$fileName . "<br />";
		        }
		    }
    		closedir($handle);
		}
	}
?>