<?php
	require_once "MakeStore.php";

	class DataBaseHelper{
		
		static private $instanceDBhelper = NULL;
		private $makeStore;
		
		// All variable stattic such as query
		private function __construct($numberDb){
			$this->makeStore = MakeStore::getStore($numberDb);
		}
		
		static public function getInstance($numberDb){
			if(DataBaseHelper::$instanceDBhelper == NULL){
				DataBaseHelper::$instanceDBhelper = new DataBaseHelper($numberDb);
			}
			return DataBaseHelper::$instanceDBhelper;	
		}
		
		/** Get all elements in the support storage */
		private function getAll($nameTable){
			$query = "SELECT * FROM $nameTable";
			return $this->makeStore->getAll($query);
		}

		/** Count all element in corrispondante table */
		private function countAll($nameTable){
			$query = "SELECT count(*) AS numRows FROM $nameTable";
			return $this->makeStore->getAll($query);
		}

		/** Get all elements in the support storage with limit and offset */
		private function getLimitedRows($nameTable, $limit, $offset){
			$query = "SELECT * FROM $nameTable  ORDER BY created_at DESC LIMIT $limit OFFSET $offset";
			return $this->makeStore->getAll($query);
		}
		
		/** Get element by id in the support storage */
		public function getById($nameTable, $id){
			$query = "SELECT * FROM $nameTable WHERE id = $id";
			return $this->makeStore->getAll($query);
		}
		
		/** insert current element in the support storage */
		private function insertInto($table){
			$tableName = $table::getNameTable();
			$keyRow = "(";
			$keyValue = "(";
			foreach(explode(';', $table->getListValues()) as $key => $value){
				if(explode(',', $value)[0] !== "id"){
					$keyRow .= explode(',', $value)[0] . ", ";
					if((explode(',', $value)[0] === "created_at" or explode(',', $value)[0] === "updated_at") and explode(',', $value)[1] ===""){
						$keyValue .= "CURRENT_TIMESTAMP, ";
					}
					else{
						$keyValue .= "'" . explode(',', $value)[1] . "', ";
					}
				}
			}
			$keyRow = trim($keyRow, ' ,');
			$keyValue = rtrim($keyValue, ' ,');
			$keyRow .= ")";
			$keyValue .= ")";
			$query = "INSERT INTO $tableName $keyRow Values$keyValue";
			return $this->makeStore->insertInto($query);
		}
		
		/** Delete current element in the support storage */
		private function deleteTo($nameTable, $id){
			$query = "DELETE FROM $nameTable WHERE id = $id";
			return $this->makeStore->deleteTo($query);
		}

		/** Delete all elements in the support storage */
		private function deleteAll($nameTable){
			$query = "DELETE FROM $nameTable";
			return $this->makeStore->deleteTo($query);
		}
		
		/** Update current element in the support storage */
		public function update($table, $id){
			$tableName = $table::getNameTable();
			$key_value = "";
			foreach(explode(';', $table->getListValues()) as $key => $value){
				if((explode(',', $value)[0] !== "id") and (explode(',', $value)[1] !=='')){
					if((explode(',', $value)[0] === "created_at" or explode(',', $value)[0] === "updated_at") and explode(',', $value)[1] ===""){
						$key_value .= explode(',', $value)[0] . "= CURRENT_TIMESTAMP, ";
					}
					else{
						$key_value .= explode(',', $value)[0] . "='" . explode(',', $value)[1] . "', ";
					}
				}
			}
			$key_value = rtrim($key_value, ' ,');
			$query = "UPDATE $tableName " . "SET $key_value WHERE id = $id";
			return $this->makeStore->insertInto($query);
		}

		/**
		 * for all Tables count row in corispondant table
		 */
		public function count($nameTable){
			return $this->countAll($nameTable);
		}

		/**
		 * for all Tables create row in corispondant table
		 */
		public function create($table){
			return $this->insertInto($table);
		}

		/**
		 * for all Tables get all rows in corispondant table
		 */
		public function getRows($nameTable){
			return $this->getAll($nameTable);
		}

		/**
		 * for all Tables get all rows with limit and offset in corispondant table
		 */
		public function getLimitOfOffsetRows($nameTable, $limit, $offset){
			return $this->getLimitedRows($nameTable, $limit, $offset);
		}

		/**
		 * for all Tables delete an element in corispondant table
		 */
		public function delete($nameTable, $id){
			return $this->deleteTo($nameTable, $id);
		}

		/**
		 * for all Tables delete all elements in corispondant table
		 */
		public function deleteElements($nameTable){
			return $this->deleteAll($nameTable);
		}

		/**
		 * For the table Users
		 */
		public function getUsersByUsername($tableName, $username){
			$query = "SELECT * FROM $tableName  WHERE username= '$username'";
			return $this->makeStore->getAll($query);
		}

		/**
		 *  For check if column existe
		 */
		public function checkColum($tableName, $column, $match){
			$query = "SELECT * FROM $tableName  WHERE $column = '$match'";
			return $this->makeStore->getAll($query);
		}

		/**
		 * For the table Teachers
		 */
		public function getCoursesOfTeacher($id){
			$query ="SELECT DISTINCT teachers.*
						FROM teachers 
						JOIN teachers_courses on teachers.id = teachers_courses.id_teacher
						JOIN courses on courses.id = teachers_courses.id_course
						WHERE courses.id = $id";
			return $this->makeStore->getAll($query);
		}

		/**
		 * For the table Teachers and Courses and reservations_active 
		 */
		public function getActiveReservations(){
			$objDate = new DateTime('now');
    		$dateNow = $objDate->format('Y-m-d');
			$query ="SELECT teachers.name as nameTea, teachers.firstname as firstnameTea, teachers.matricola,
							courses.name, courses.codice,
							reservations_active.reservation_day, reservations_active.reservation_hour  
						FROM teachers 
						JOIN reservations_active on teachers.id = reservations_active.id_teacher
						JOIN courses on courses.id = reservations_active.id_course
						WHERE reservations_active.reservation_day >= '$dateNow'";
			return $this->makeStore->getAll($query);
		}

		/**
		 * Get all reservation make use by admin
		 */
		public function getAllReservationsByAdmin(){
			$objDate = new DateTime('now');
    		$dateNow = $objDate->format('Y-m-d');
			$query = "SELECT DISTINCT teachers.name as nameTea, teachers.firstname as firstnameTea, teachers.matricola,
							 courses.name, courses.codice, res.id as reservation, res.status_reservation as stato, user.username,
							 reservations_active.id, reservations_active.reservation_day, reservations_active.reservation_hour  
					  FROM teachers 
					  JOIN reservations_active ON teachers.id = reservations_active.id_teacher
					  JOIN courses ON courses.id = reservations_active.id_course
					  JOIN reservations as res ON res.id_reservations_active = reservations_active.id
					  JOIN users as user ON user.id = res.id_user
					  WHERE reservations_active.reservation_day >= NOW() AND res.id IN (
						  SELECT DISTINCT reservations.id 
						  FROM reservations_active as reservations_active1
						  JOIN reservations ON reservations_active1.id = reservations.id_reservations_active 
						  JOIN users ON reservations.id_user = users.id 
						  WHERE reservations_active1.reservation_day >= NOW()
						)";
			return $this->makeStore->getAll($query);
		}

		/**
		 * For the table Teachers, ActiveReservations and Reservations 
		 */
		public function getHoursDispoOfTeacher($idcourse, $idTeacher, $date){
			$objDate = new DateTime('now');
    		$dateNow = $objDate->format('Y-m-d');
			$query ="SELECT reservations_active.reservation_hour as hour
						FROM reservations_active
						WHERE reservations_active.reservation_day = '$date' AND 
							  reservations_active.id_course = '$idcourse' AND 
							  reservations_active.id_teacher = '$idTeacher' AND
							  reservations_active.reservation_day >= '$dateNow'";
			return $this->makeStore->getAll($query);
		}

		/**
		 * For the table Teachers and Courses 
		 */
		public function getNoAssCoursesOfTeacher($idTeacher){
			$query ="SELECT courses1.id, courses1.name, courses1.codice
						FROM courses as courses1
						WHERE courses1.id NOT IN (SELECT courses.id
												  FROM courses
												  JOIN teachers_courses ON courses.id = teachers_courses.id_course
												  JOIN teachers ON teachers_courses.id_teacher = teachers.id
												  WHERE teachers.id = '$idTeacher')";
			return $this->makeStore->getAll($query);
		}

		/**
		 *  Check if esiste link between Teacher of Course
		*/
		public function checkEsisteLinkTeacherCourse($idTeacher, $idcourse){
			$query = "SELECT * FROM teachers_courses WHERE id_teacher = '$idTeacher' AND id_teacher = '$idcourse'";
			return $this->makeStore->getAll($query);
		}

		/**
		 *  Get all reservations for an user by username
		*/
		public function getAllReservations($username){
			$objDate = new DateTime('now');
    		$dateNow = $objDate->format('Y-m-d');
			$query = "SELECT DISTINCT teachers.name as nameTea, teachers.firstname as firstnameTea, teachers.matricola,
							 courses.name, courses.codice, res.id as reservation, res.status_reservation as stato,
							 reservations_active.id, reservations_active.reservation_day, reservations_active.reservation_hour  
					  FROM teachers 
					  JOIN reservations_active ON teachers.id = reservations_active.id_teacher
					  JOIN courses ON courses.id = reservations_active.id_course
					  JOIN reservations as res ON res.id_reservations_active = reservations_active.id
					  WHERE reservations_active.reservation_day >= NOW() AND res.id IN (
						  SELECT DISTINCT reservations.id 
						  FROM reservations_active as reservations_active1
						  JOIN reservations ON reservations_active1.id = reservations.id_reservations_active 
						  JOIN users ON reservations.id_user = users.id 
						  WHERE users.username = '$username' AND reservations_active1.reservation_day >= NOW()
						)";
			return $this->makeStore->getAll($query);
		}

		/**
		 * 
		 */
		public function getAllReservationsStories($username){
			$objDate = new DateTime('now');
    		$dateNow = $objDate->format('Y-m-d');
			$query = "SELECT DISTINCT teachers.name as nameTea, teachers.firstname as firstnameTea, teachers.matricola,
							 courses.name, courses.codice, res.id as reservation, res.status_reservation as stato,
							 reservations_active.id, reservations_active.reservation_day, reservations_active.reservation_hour  
					  FROM teachers 
					  JOIN reservations_active ON teachers.id = reservations_active.id_teacher
					  JOIN courses ON courses.id = reservations_active.id_course
					  JOIN reservations as res ON res.id_reservations_active = reservations_active.id
					  WHERE reservations_active.reservation_day < '$dateNow' AND res.id IN (
						  SELECT DISTINCT reservations.id 
						  FROM reservations_active as reservations_active1
						  JOIN reservations ON reservations_active1.id = reservations.id_reservations_active 
						  JOIN users ON reservations.id_user = users.id 
						  WHERE users.username = '$username' AND reservations_active1.reservation_day < '$dateNow'
						)";
			return $this->makeStore->getAll($query);
		}

		/**
		 *  Get free active reservations
		 */
		public function getFreeActiveReservationsBefore(){
			$query = "SELECT DISTINCT teachers.name as nameTea, teachers.firstname as firstnameTea, teachers.matricola,
							 courses.name, courses.codice,
							 reservations_active.id, reservations_active.reservation_day, reservations_active.reservation_hour  
					  FROM teachers 
					  JOIN reservations_active on teachers.id = reservations_active.id_teacher
					  JOIN courses on courses.id = reservations_active.id_course
					  WHERE reservations_active.reservation_day >= NOW() AND reservations_active.id NOT IN (
						  SELECT DISTINCT reservations_active1.id 
						  FROM reservations_active as reservations_active1
						  JOIN reservations ON reservations_active1.id = reservations.id_reservations_active
						  WHERE reservations_active1.reservation_day >= NOW()
						)";
			return $this->makeStore->getAll($query);
		}

		/**
		 *  Get free active reservations
		 */
		public function getFreeActiveReservationsAfter($username){
			$query = "SELECT DISTINCT teachers.name as nameTea, teachers.firstname as firstnameTea, teachers.matricola,
							 courses.name, courses.codice, courses.id as course,
							 reservations_active.id, reservations_active.reservation_day, reservations_active.reservation_hour  
					  FROM teachers 
					  JOIN reservations_active ON teachers.id = reservations_active.id_teacher
					  JOIN courses ON courses.id = reservations_active.id_course
					  WHERE reservations_active.reservation_day >= NOW() AND reservations_active.id NOT IN (
						  SELECT DISTINCT reservations_active1.id 
						  FROM reservations_active as reservations_active1
						  JOIN reservations ON reservations_active1.id = reservations.id_reservations_active
						  JOIN users ON reservations.id_user = users.id 
						  WHERE reservations_active1.reservation_day >= NOW() 
						  		AND (users.username = '$username' 
								OR reservations.status_reservation = 'ACTIVATED')
					  )";
			return $this->makeStore->getAll($query);
		}

		/**
		*  Get free active reservations
		*/
		public function checkReservations($activeReservation, $user){
			$query = "SELECT reservations.*
					  FROM reservations
					  JOIN reservations_active ON reservations_active.id = reservations.id_reservations_active 
					  JOIN users ON users.id = reservations.id_user
					  WHERE reservations_active.id = '$activeReservation' AND reservations.status_reservation = 'ACTIVATED' AND users.username != '$user'";
			return $this->makeStore->getAll($query);
		}

		/**
		*  Get free active reservations
		*/
		public function checkReservationsBeforeCreate($idReservation){
			$query = "SELECT id
					  FROM reservations
					  WHERE id_reservations_active = '$idReservation' AND status_reservation = 'ACTIVATED'";
			return $this->makeStore->getAll($query);
		}

		/**
		 * Get all message of users
		 */
		public function getAllFlashUser($username){
			$query = "SELECT inbox.*
					  FROM inbox
					  JOIN users ON users.id = inbox.id_user
					  WHERE users.username = '$username'
					  ORDER BY created_at DESC";
			return $this->makeStore->getAll($query);
		}
	}

?>