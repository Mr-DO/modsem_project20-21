<?php 
	interface Store{
		
		/** Get all elements in the support storage */
		public function getAll($query);
		
		/** insert current element in the support storage */
		public function insertInto($query);
		
		/** Delete current element in the support storage */
		public function deleteTo($query);
		
		/** Update current element in the support storage */
		public function update($query);
		
	}
?>