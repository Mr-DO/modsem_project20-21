<?php 
	require_once "DAOFactory.php";

	class PdODAOFactory extends DAOFactory{
		
		/**
		*@ Server paramaters
		*/
		static $SERVER = array(
			'host' => 'localhost',
			'database' => 'db_app_client',
			'username' => 'root',
			'password' => '');

		/**
		 * Method for create connection
		 * @return object connection
		 */

		public function createConnection(){
			$server = PdODAOFactory::$SERVER;
			$conn = NULL;
			try{
				$conn = new PDO("mysql:host=".$server['host'].";dbname=".$server['database'], $server['username'], $server['password']);
				$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			}catch(PDOException $e){
				echo 'ERROR: ' . $e->getMessage();
				return NULL;
			}   
			return $conn;
		}
		
	}
?>

