<?php 

    require_once '../../../functions/index.php';

    $uploadOk = 1;
    $old_target_file = '';
    $target_dir = "../../../image/profileImg/";
    $target_file = $target_dir . basename($_FILES["addProfileImg"]["name"]);
    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

    if(isset($_POST['username']) and !empty($_POST['username'])){

        // old base name image profile without extension
        $old_target_file = $target_dir . $_POST['username'];
        $old_target_file = checkUserProfile($old_target_file);

        $target_file = $target_dir . $_POST['username'];

        // Check if image file is a actual image or fake image
        if(isset($_POST["sumbitAddProfileImg"])) {
            $check = getimagesize($_FILES["addProfileImg"]["tmp_name"]);
            if($check !== false) {
                $uploadOk = 1;
            } else {
                $uploadOk = 0;
            }
        }

        // Check file size
        if ($_FILES["addProfileImg"]["size"] > 200000) {
            $uploadOk = 0;
        }

        // Allow certain file formats
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ) {
            $uploadOk = 0;
        }

        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            $_SESSION['profileImage'] = "Il caricamento dell'immagine di profilo non è avvenuto con successo."; 
        } else {
            $target_file .= '.' . $imageFileType; 
            if (move_uploaded_file($_FILES["addProfileImg"]["tmp_name"], $target_file)) {
                $_SESSION['profileImage'] = "Il caricamento dell'immagine di profilo è avvenuto con successo.";
                if($old_target_file !== null) unlink($old_target_file);
            } else {
                $_SESSION['profileImage'] = "Il caricamento dell'immagine di profilo non è avvenuto con successo."; 
            }
        }
        header('Location: ' . '../../../home/index.php?subvar=update-img-profile');
    }
    else{
        $_SESSION['profileImage'] = "Richiesta non tratatta, riprova piu tardi.";
        header('Location: ' . '../../../home/index.php?subvar=update-img-profile');
    }
?>