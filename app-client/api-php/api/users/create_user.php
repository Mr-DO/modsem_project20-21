<?php

    // required headers
    header("Access-Control-Allow-Origin: http://localhost/tweb-project/*");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

    require_once '../../DAO/DataBaseHelper.php';
    require_once '../../model/table/Users.php';
    require_once '../../../functions/index.php';
 
    // database connection will be here
    $dataBaseHelper = DataBaseHelper::getInstance(1);

    // get posted data
    $username = json_decode($_POST['username']);
    $password = json_decode($_POST['password']);

    //Control data
    $username = htmlspecialchars(strip_tags($username));
    $password = password_hash(htmlspecialchars(strip_tags($password)), PASSWORD_BCRYPT);
    
    // set product property values
    $matricola = getnewMatricola('STD', 10, $dataBaseHelper, 'users');
    $user= new Users(0, $username, $matricola, $password, '', '', '');

    if($dataBaseHelper->create($user)){
 
        // set response code
        http_response_code(200);
     
        // display message: user was created
        echo json_encode(array('message' => 'Utente creato, con matricola : ' . $matricola . '.', 'username' => $username));
    }
     
    // message if unable to create user
    else{
     
        // set response code
        http_response_code(400);
     
        // display message: unable to create user
        echo json_encode(array('message' => 'non sei abilitato a creare un utente.'));
    }
?>