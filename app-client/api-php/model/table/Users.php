<?php 
	
require_once "Tables.php";
class Users implements Tables{
	/* id 	username 	matricola 	password 	name 	firstname 	isAdmin 	created_at 	updated_at */
	static private $TABLE_NAME = "users";
	private $id;
	private $username;
	private $matricola;
	private $password;
	private $name;
	private $firstname;
	private $isAdmin;
	private $created_at = "";
	private $updated_at = "";
	
	public function __construct($id, $username, $matricola, $password, $name, $firstname, $isAdmin){
	
		$this->id = $id;
		$this->username = $username;
		$this->matricola = $matricola;
		$this->password = $password;
		$this->name = $name;
		$this->firstname = $firstname;
		$this->isAdmin = $isAdmin;
	}
	
	// all getters
	public function getId(){ return $this->id; }
	public function getUserName(){ return $this->username; }
	public function getMatricola(){ return $this->matricola; }
	public function getPassword(){ return $this->password; }
	public function getName(){ return $this->name; }
	public function getFirstName(){ return $this->firstname; }
	public function getIsAdmin(){ return $this->isAdmin; }
	public function getCreatedDate(){ return $this->created_at; }
	public function getUpdatedDate(){ return $this->updated_at; }
	
	// all setters
	public function setId($id){ $this->id = $id; }
	public function setUserName($username){ $this->username = $username; }
	public function setMatricola($matricola){ $this->matricola = $matricola; }
	public function setPassword($password){ $this->password = $password; }
	public function setName($name){ $this->name = $name; }
	public function setFirstName($firstname){ $this->firstname = $firstname; }
	public function setIsAdmin($isAdmin){ $this->isAdmin = $isAdmin; }
	public function setCreatedDate($createdDate){ $this->created_at = $createdDate; }
	public function setUpdatedDate($updatedDate){ $this->updated_at = $updatedDate; }
	
	/*@ get name table*/
	static public function getNameTable(){
		return Users::$TABLE_NAME;
	}
	/*id 	username 	password 	name 	firstname 	isAdmin 	created_at 	updated_at */
	/*@ get list values of the table*/
	public function getListValues(){
		return	"id,".$this->getId().
				";username,".$this->getUserName().
				";matricola,".$this->getMatricola().
				";password,".$this->getPassword().
				";name,".$this->getName().
				";firstname,".$this->getFirstName().
				";isAdmin,".$this->getIsAdmin().
				";created_at,".$this->getCreatedDate().
				";updated_at,".$this->getUpdatedDate();
	}
	
}

?>