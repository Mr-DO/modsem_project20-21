<?php
    //start session
    require_once 'session.php';

    require_once '../api-php/DAO/DataBaseHelper.php';
    require_once '../api-php/model/table/Users.php';
    require_once '../config/core.php';

    require_once '../vendor/autoload.php';

    use \Firebase\JWT\JWT;

    if(!isset($redirect_success)) $redirect_success = "http://localhost/tweb-project/manut";
    if(!isset($redirect_fail))  $redirect_fail = "http://localhost/tweb-project/views/login.php";
    if(!isset($redirect_fail_root)) $redirect_fail_root = "http://localhost/tweb-project";
    
    unset($_SESSION['error']);
    $global_username = '';

    if(isset($_COOKIE['jwt']) and !empty($_COOKIE['jwt'])){
        $jwt = $_COOKIE['jwt'];

        try {
            // decode jwt
            $decoded = JWT::decode($jwt, $key, array('HS256'));

            // set user property values here
            $decoded = (array) $decoded;
            $decoded =  ((array) $decoded["data"]);

            $username = $decoded['username'];
            $password = $decoded['password'];
            $global_username = $username;
            // Get instance of dataBase
            $dataBaseHelper = DataBaseHelper::getInstance(1);
            $checkUser = $dataBaseHelper->getUsersByUsername(Users::getNameTable(), $username);

            if(count($checkUser) == 1){
                $checkUser = $checkUser[0];
                if(($password !== $checkUser["password"]) or ($checkUser["isAdmin"] !== '1')){
                    $_SESSION["error"]["message"] = "accesso negato fai login.";
                    $_SESSION["error"]["username"] = $username;
                    header("Location: $redirect_fail");
                    exit();
                }
            }else{
                //coockie is wrong
                header("Location: $redirect_fail_root");
                exit();
            }
            setcookie("jwt", $jwt, time()+1800, "/", "localhost");
            if(!empty($redirect_success)){
                header("Location: $redirect_success");
                exit();
            }
        }catch(Exception $e){
            $_SESSION["error"]["message"] = "riquiesta fallita riprova ulteriamente.";
            $_SESSION["error"]["username"] = $decoded["username"];
            header("Location: $redirect_fail");
            exit();
        }

    }else{
        //suspect request
        header("Location: $redirect_fail_root");
        exit();
    }


    /*if(isset($path_session) and !empty($path_session)) require_once 'session.php'; else require_once $path_session;

    if(isset($path_database_helper) and !empty($path_database_helper)) require_once '../api-php/DAO/DataBaseHelper.php'; else require_once $path_database_helpe;
    if(isset($path_users) and !empty($path_users)) require_once '../api-php/model/table/Users.php'; else require_once $path_users;
    if(isset($path_core) and !empty($path_core)) require_once '../config/core.php'; else require_once $path_core;

    if(isset($path_jwt) and !empty($path_jwt)) require_once '../vendor/autoload.php'; else require_once $path_jwt;
     */
?>