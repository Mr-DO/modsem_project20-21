<?php

    $redirect_fail_root = "http://localhost/app-client";

    if(isset($_COOKIE['jwt']) and !empty($_COOKIE['jwt'])){
        unset($_COOKIE['jwt']);
        setcookie('jwt', null, time()-3600, "/", "localhost");
    }
    if(isset($_SESSION) and !empty($_SESSION)){
        session_destroy();
    }

    header("Location: $redirect_fail_root");
    exit();
?>