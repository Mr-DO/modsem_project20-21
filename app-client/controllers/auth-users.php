<?php
    //start session
    require_once 'session.php';

    require_once '../api-php/DAO/DataBaseHelper.php';
    require_once '../api-php/model/table/Users.php';
    require_once '../config/core.php';

    require_once '../vendor/autoload.php';

    use \Firebase\JWT\JWT;

    $redirect_success = "../home";
    $redirect_fail = "../views/login.php";
    unset($_SESSION['error']);

    if(isset($_COOKIE['jwt']) and !empty($_COOKIE['jwt']) and !(isset($_POST) and !empty($_POST))){
        $jwt = $_COOKIE['jwt'];

        try {
            // decode jwt
            $decoded = JWT::decode($jwt, $key, array('HS256'));

            // set user property values here
            $decoded = (array) $decoded;
            $decoded =  ((array) $decoded["data"]);

            setcookie("jwt", $jwt, time()+1800, "/", "localhost");
            header("Location: $redirect_success");
            exit();
        }catch(Exception $e){
            $_SESSION["error"]["message"] = "riquiesta fallita riprova ulteriamente.";
            $_SESSION["error"]["username"] = $decoded["username"];
            header("Location: $redirect_fail");
            exit();
        }

    }
    else if(isset($_POST) and !empty($_POST)){
        $dataBaseHelper = DataBaseHelper::getInstance(1);
        $username = htmlspecialchars(strip_tags($_POST['username']));
        $password = htmlspecialchars(strip_tags($_POST['password']));

        $checkUser = $dataBaseHelper->getUsersByUsername(Users::getNameTable(), $username);

        if(count($checkUser) == 1){
            $checkUser = $checkUser[0];
            if(password_verify($password, $checkUser["password"]) and ($checkUser["isAdmin"]==='0')){
                $token = array(
                    "iss" => $iss,
                    "aud" => $aud,
                    "iat" => $iat,
                    "nbf" => $nbf,
                    "data" => array(
                        "id" => $checkUser['id'],
                        "username" => $checkUser["username"],
                        "password" => $checkUser["password"],
                        "isAdmin" => $checkUser["isAdmin"]
                    )
                );

                // generate jwt
                $jwt = JWT::encode($token, $key);
                setcookie("jwt", $jwt, time()+60, "/", "localhost");
                header("Location: $redirect_success");
                exit();
            }
            else{
                $_SESSION["error"]["message"] = "username o password errata.";
                $_SESSION["error"]["username"] = $username;
                header("Location: $redirect_fail");
                exit;
            }
        }
        else{
            $_SESSION["error"]["message"] = "L'utente non esiste.";
            $_SESSION["error"]["username"] = $username;
            header("Location: $redirect_fail");
            exit();
        }
    }
    else{
        // redirect home user no logged.
        $_SESSION["error"]["message"] = "Non sei abilitato a fare questa riquesta.";
        $_SESSION["error"]["username"] = "";
        header("Location: $redirect_fail");
        exit();
    }

?>