<?php 
	// The life initiate one and finished one day. never give up stand up for buid your life.
	if(isset($_GET['subvar']) and !empty($_GET['subvar'])){
		$subvar = $_GET['subvar'];
	}else{
		$subvar = "";
	}
?>

<!DOCTYPE html>
<html>	
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>Home</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" type="text/css" href="css/main.css" />
		<script type="text/javascript" src="js/main.js"></script>
		<script type="text/javascript" src="js/ajax.js"></script>
		<script type="text/javascript" src="js/show-all-imm.js"></script>
  		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
	</head>
	<body>
		<main class="class-body">
			<header class="class-header"> 
				<?php require_once("components/header.php"); ?>
			</header>
			<div class="class-elements-midlle">
				<div class="left-menu"> 
					<?php require_once("components/left_home.php"); ?>
				</div>
				<div class=div-sections> 
					<div class="alert-message"> </div>
					<div class="content"> 
					<?php
							switch($subvar){
								case "contact":
									require_once("components/contacts.php");
									break;
								case "ref":
									require_once("components/referimenti.php");
									break;
								case "showAll":
									require_once("views/show_all.php");
									break;
								case "ricercaAvanzata":
									require_once("views/ricerca_avanzata.php");
									break;
								case "immAfToEu":
									require_once("views/imm_af_to_eu.php");
									break;
								case "immEuToAf":
									require_once("views/imm_eu_to_af.php");
									break;
								case "immAllToIt":
									require_once("views/imm_all_to_it.php");
									break;
								case "immItToAll":
									require_once("views/imm_it_to_all.php");
									break;
								case "immItToUS":
									require_once("views/imm_it_to_us.php");
									break;
								default :
									require_once("components/about.php");
									break;									
							}
						?>
					</div>
				</div>
				<aside class=class-aside>
					<?php require_once("components/aside.php"); ?>
				</aside>
			</div>
			<footer class="class-footer">
				<?php require_once("components/footer.php"); ?>
			</footer>
		</main>
	</body>
</html>
<script type="text/javascript" src="js/users/visited.js"></script>
