<?php
// show error reporting
error_reporting(E_ALL);
 
// set your default time-zone
date_default_timezone_set('Europe/Rome');
 
// variables used for jwt
$key = "tweb-project_key";
$iss = "http://tweb-project.org";
$aud = "http://tweb-project.com";
$iat = 1356999524;
$nbf = 1357000000;
?>