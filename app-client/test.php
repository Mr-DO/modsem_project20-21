<?php 
    require_once 'vendor/autoload.php';
    require_once 'api-php/DAO/DataBaseHelper.php';
    //require_once 'api-php/model/table/DataBaseHelper.php';
    
    use \Firebase\JWT\JWT;
    //require_once 'api-php/model/Hours.php';

    $faker = Faker\Factory::create();

    $key = "example_key";
    $token = array(
        "iss" => "http://example.org",
        "aud" => "http://example.com",
        "iat" => 1356999524,
        "nbf" => 1357000000
    );
    
    /**
     * IMPORTANT:
     * You must specify supported algorithms for your application. See
     * https://tools.ietf.org/html/draft-ietf-jose-json-web-algorithms-40
     * for a list of spec-compliant algorithms.
     */
    $jwt = JWT::encode($token, $key);
    $decoded = JWT::decode($jwt, $key, array('HS256'));
    
    print_r($decoded);
    
    /*
     NOTE: This will now be an object instead of an associative array. To get
     an associative array, you will need to cast it as such:
    */
    
    $decoded_array = (array) $decoded;

    print_r($decoded_array);
    echo "firt faker : " . $faker->name . "<br />";

    //echo Hours::ORARIO_7;

    $objDate = new DateTime('now');
    
    echo $objDate->format('Y-m-d H:i:s');

    // database connection will be here
    $dataBaseHelper = DataBaseHelper::getInstance(1);
    $hours = $dataBaseHelper -> getHoursDispoOfTeacher(1, 1, date_create('2019-04-26')->format('Y-m-d'));
    var_dump($hours);
    echo "<br />";

    // =>
    var_dump($dataBaseHelper -> getNoAssCoursesOfTeacher(2));
?>


INSERT DATA { 
    GRAPH <http://www.semanticweb.org/ousmane/modsemproject20-21> { 
        imm:Ousmane5Diakite imm:haImmigratoPer imm:MotivoDiLavoro;
                            imm:haNazioneDiOrigine imm:Italia;
                            imm:haNazioneDiResidenza imm:Costa-DAvorio;
                            imm:haNome "Ousmane5";
                            imm:hacognome "DIAKITE5";
                            imm:haImmigratoInAnno 2014.
    }
}