CREATE DATABASE db_app_client;

CREATE TABLE users (
    id          INT             AUTO_INCREMENT  ,
    username    VARCHAR(255)                    UNIQUE,
    matricola   VARCHAR(10)                     UNIQUE,
    password    VARCHAR(255)                    ,
    name        VARCHAR(255)                    ,
    firstname   VARCHAR(255)                    ,
    isAdmin     TINYINT(1)             NOT NULL DEFAULT 0,
    created_at  DATETIME                        DEFAULT CURRENT_TIMESTAMP,
    updated_at  DATETIME                        DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
)   ENGINE=INNODB;