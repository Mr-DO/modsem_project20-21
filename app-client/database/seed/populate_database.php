<?php
    require_once '../../vendor/autoload.php';
    require_once '../../api-php/DAO/DataBaseHelper.php';
    require_once '../../api-php/model/table/Users.php';
    require_once '../../functions/index.php';

    use \Firebase\JWT\JWT;

    // get instace of faker
    $faker = Faker\Factory::create();

    // database connection will be here
    $dataBaseHelper = DataBaseHelper::getInstance(1);

    // create three users
    function create_users_admin($dataBaseHelper, $faker){
        $dataBaseHelper->deleteElements(Users::getNameTable());

        $username_1 = "menezMrDO-1"; $password_1 = password_hash("generalpass-1", PASSWORD_BCRYPT); $matricola_1 = getnewMatricola('STD', 7, $dataBaseHelper, 'users');
        $user_1= new Users(0, $username_1, $matricola_1, $password_1, $faker->firstName, $faker->lastName, '0');

        $username_2 =  "menezMrDO-2"; $password_2 = password_hash("generalpass-2", PASSWORD_BCRYPT); $matricola_2 = getnewMatricola('STD', 7, $dataBaseHelper, 'users');
        $user_2= new Users(0, $username_2, $matricola_2, $password_2, $faker->firstName, $faker->lastName, '0');

        $username = "menezAdmin"; $password = password_hash("generalpass-3", PASSWORD_BCRYPT); $matricola = getnewMatricola('ADM', 7, $dataBaseHelper, 'users');
        $user= new Users(0, $username, $matricola, $password, $faker->firstName, $faker->lastName, '1');

        $dataBaseHelper->create($user_1);
        $dataBaseHelper->create($user_2);
        $dataBaseHelper->create($user);
    }

    // create reservations make by users
    create_users_admin($dataBaseHelper, $faker);

    echo "Done! your are populate you data base.";

?>