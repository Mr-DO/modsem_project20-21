/**
 *  Game, the player you must find the wordl you see in the screen
 */
var allWord = [
    'treno',
    'aero',
    'strada',
    'casa',
    'viaggio',
    'mondo',
    'speranza',
    'film',
    'ragazza',
    'parola',
    'piscina',
    'velo'
]
var cont = 0;
var limit = 5;
var wordSelect = '';
var wordSelectCache = '';
var arrayFailSucces = [
    'answer-1',
    'answer-2',
    'answer-3',
    'answer-4',
    'answer-5'
];

function startGame(){
    document.getElementById('start-game').addEventListener('click', function(event){
        initGlobalVariable();
        event.target.style.display = "none";
    });
}

function initGlobalVariable(){
    document.getElementById('div-play-game').style.display = "";
    document.getElementById("answer-input").value = '';
    document.getElementById("answer-input").disabled = false;
    wordSelect = randomElmtFrom1Array(allWord);
    wordSelectCache = '';
    cont = 0;
    for(var i=0; i<wordSelect.length; i++){
        wordSelectCache += '*';
    }
    document.getElementById('find-word').innerHTML = wordSelectCache;
    document.getElementById('ouput-message-game').style.display = "none";
}

function restoreFailNotification(){
    for(var i = 0; i < arrayFailSucces.length; i++){
        document.getElementById(arrayFailSucces[i]).style.backgroundColor = 'white';
    }
}

function randomElmtFrom1Array(items){
    return items[Math.floor(Math.random() * items.length)];
}

function validCharIncomming(elmtInput){
    let inputStr = elmtInput.value;
    let inputLast = inputStr.slice(-1)
    if(!(/^([a-zA-Z]{1})$/.test(inputLast))) {
        elmtInput.value = inputStr.replace(inputLast, '');
        return false;
    }
    return true;
}

startGame();
document.getElementById("answer-input").addEventListener('input', function(event){
    if(validCharIncomming(event.target)){
        var valueIncomming = event.target.value.toLowerCase();
        if(valueIncomming === wordSelect){
            event.target.disabled = true;
            var j = 0;
            var replaceWordSelectCache = '';
            for(j =0; j < valueIncomming.length; j++){
                replaceWordSelectCache += valueIncomming[j];
            }
            for(j; j < wordSelectCache.length; j++){
                replaceWordSelectCache += '*';
            }
            document.getElementById('find-word').innerHTML = replaceWordSelectCache;
            document.getElementById('result-game').innerHTML = "Bravo hai indovinato la parola.";
            document.getElementById('result-game').style.color = 'green';
            document.getElementById('ouput-message-game').style.display = "";
        }
        else if(!wordSelect.startsWith(valueIncomming)){
            document.getElementById(arrayFailSucces[cont++]).style.backgroundColor = 'red';
        }
        else{
            var j = 0;
            var replaceWordSelectCache = '';
            for(j =0; j <valueIncomming.length; j++){
                replaceWordSelectCache += valueIncomming[j];
            }
            for(j; j<wordSelectCache.length; j++){
                replaceWordSelectCache += '*';
            }
            document.getElementById('find-word').innerHTML = replaceWordSelectCache;
        }
        if(cont >= limit){
            document.getElementById(arrayFailSucces[--cont]).style.backgroundColor = 'red';
            event.target.disabled = true;
            document.getElementById('result-game').innerHTML = "Non hai indovinato, la parola era \" " + wordSelect + "\" puoi riprovare.";
            document.getElementById('result-game').style.color = 'red';
            document.getElementById('ouput-message-game').style.display = "";
        }
    }
    if(cont >= limit){
        document.getElementById(arrayFailSucces[--cont]).style.backgroundColor = 'red';
        event.target.disabled = true;
        document.getElementById('result-game').innerHTML = "Non hai indovinato, la parola era : \" " + wordSelect + "\" puoi riprovare.";
        document.getElementById('result-game').style.color = 'red';
        document.getElementById('ouput-message-game').style.display = "";
    }
});

document.getElementById('replay-game').addEventListener('click', function(event){
    restoreFailNotification();
    initGlobalVariable();
});