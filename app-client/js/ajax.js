
// Execute AJAX POST
function ajaxPost(url, data, callback_succes, callback_fail, isJson) {
    var req = new XMLHttpRequest();
    req.open("POST", url);
    req.addEventListener("load", function () {
        if (req.status >= 200 && req.status < 400) {
            // call function on succes for do somethings with response
            callback_succes(req.responseText);
        } else {
            // call function on fail for do somethings with response
            callback_fail(req.responseText);
        }
    });
    req.addEventListener("error", function () {
        console.error("Mal formed URL " + url);
    });
    if (isJson) {
        // Ddefine content JSON
        req.setRequestHeader("Content-Type", "application/json");
        // Transform json format in data string
        data = JSON.stringify(data);
    }
    req.send(data);
}

// Execute AJAX GET
function ajaxGet(url, callback_succes, callback_fail) {
    var req = new XMLHttpRequest();
    req.open("GET", url);
    req.addEventListener("load", function () {
        if (req.status >= 200 && req.status < 400) {
            // call function for do somethings with response
            callback_succes(req.response);
        } else {
            // call function for do somethings with response
            callback_fail(req.response);
        }
    });
    req.addEventListener("error", function () {
        console.error("Mal formed URL " + url);
    });
    req.send(null);
}