/**
 * function with request ajax for changereservation status
 * 
 */

function do_reservation(user, reservation, value, mode){
    var formDatapost = new FormData();
            formDatapost.append('user', JSON.stringify(user));
            formDatapost.append('reservation', JSON.stringify(reservation));
            formDatapost.append('value', JSON.stringify(value));
            formDatapost.append('mode', JSON.stringify(mode));
            console.log('user : ' + user + ' reservation : '+ reservation + ' value : ' + value + ' mode : ' + mode);
    ajaxPost("../api-php/api/reservations/update_reservation.php", formDatapost, function callback_succes(reqMessage){
        console.log(reqMessage);
        var reqObjt = JSON.parse(reqMessage);
        alert(reqObjt.message);
        location.reload();
    }, function callback_fail(reqMessage){
        console.log(reqMessage);
        var reqObjt = JSON.parse(reqMessage);
        alert(reqObjt.message);
    }, false);
}
