/**
 * request ajax for get courses
 * 
 */
var messageFormResponse = null;
var form = null; 

if(document.getElementById("ass-teacher") !== null){
    messageFormResponse = document.getElementById("message-ass-courses");
    form = document.getElementById("form-ass-courses");
    document.getElementById("ass-teacher").addEventListener("change", function(event){
        if((document.getElementById("ass-teacher").value.length) > 0){
            var idTeacher = document.getElementById("ass-teacher").value;
            var elmtCourse = document.getElementById("ass-course");
            var listChildNodes = elmtCourse.querySelectorAll("option");
            ajaxGet("../api-php/api/courses/get_courses.php?idTeacher=" + idTeacher, function(reqMessage){
                var objResponse = JSON.parse(reqMessage);
                var courses = objResponse.courses;
                listChildNodes.forEach((node) => {
                        if(node.tagName === 'OPTION' && node.value !== '') elmtCourse.removeChild(node);
                    }
                );
                courses.forEach(element => {
                    var elmtOption = document.createElement('OPTION');
                    elmtOption.setAttribute("value", element[0]);
                    elmtOption.appendChild(document.createTextNode(element[1]));
                    elmtCourse.appendChild(elmtOption);
                });
                setTimeout(function(){
                    messageFormResponse.innerHTML = "";
                    messageFormResponse.style.display = "none";
                }, 2000);
            }, function(reqMessage){
                var messageFormResponse = document.getElementById("message-ass-courses");
                messageFormResponse.style.border = "solid 2px red";
                messageFormResponse.style.display = "";
                messageFormResponse.style.backgroundColor = "darkgrey";
                messageFormResponse.style.color = "red";
                var reqObjt = JSON.parse(reqMessage);
                messageFormResponse.innerHTML = reqObjt.message;
                listChildNodes.forEach((node) => {
                        if(node.tagName === 'OPTION' && node.value !== '') elmtTeacher.removeChild(node);
                    }
                );
                setTimeout(function(){
                    messageFormResponse.innerHTML = "";
                    messageFormResponse.style.display = "none";
                }, 2000);
            });
        }
    });
}

/**
 * request ajax for create link between teachers and courses
 * 
 */

if(document.getElementById("submit-ass-course") !== null){
    messageFormResponse = document.getElementById("message-ass-courses");
    form = document.getElementById("form-ass-courses");
    document.getElementById("submit-ass-course").addEventListener("click", function(event){
        event.preventDefault();

        var elmtCourse = document.getElementById("ass-course");
        var idCourseAss = elmtCourse.options[elmtCourse.selectedIndex].value;

        var elmtTeacher= (document.getElementById("ass-teacher"));
        var idTeacherAss = elmtTeacher.options[elmtTeacher.selectedIndex].value;

        if(required_value(elmtCourse) && required_value(elmtTeacher)){
            var formDatapost = new FormData();
            formDatapost.append('id-course', JSON.stringify(idCourseAss));
            formDatapost.append('id-teacher', JSON.stringify(idTeacherAss));

            ajaxPost("../api-php/api/teachersLinkCourses/teacher_link_course.php", formDatapost, callback_succes, callback_fail, false);
        }
    });
}