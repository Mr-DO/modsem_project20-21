/**
 * request ajax for create teachers
 * 
 */
var messageFormResponse = null;

if(document.getElementById("message-add-teacher") !== null){
    messageFormResponse = document.getElementById("message-add-teacher");
    var form = document.getElementById("form-add-teacher");
    document.getElementById("submit-teacher").addEventListener("click", function(event){
        event.preventDefault();
        var nameTeacher = document.getElementById("name-teacher");
        var firstNmaeTeacher = document.getElementById("first-name-teacher");
        var desTeacher = document.getElementById("des-teacher");
        nameTeacher.style.border = "";
        firstNmaeTeacher.style.border = "";
        desTeacher.style.border = "";
        if(controlFormRegexLength(nameTeacher, /^([a-zA-Z0-9 _,.;]{3,})$/, 3) && 
            controlFormRegexLength(firstNmaeTeacher, /^([a-zA-Z0-9 _,.;]{3,})$/, 3) &&
            controlFormRegexLength(desTeacher, /^([a-zA-Z0-9 _,.;]{3,})$/, 3)){
            var formDatapost = new FormData();
            formDatapost.append('name-teacher', JSON.stringify(nameTeacher.value));
            formDatapost.append('first-name-teacher', JSON.stringify(firstNmaeTeacher.value));
            formDatapost.append('des-teacher', JSON.stringify(desTeacher.value));

            ajaxPost("../api-php/api/teachers/create_teacher.php", formDatapost, callback_succes, callback_fail, false);
        }
    });
}

/**
 * request ajax for update teachers
 * 
 */
if(document.getElementById("message-update-teacher") !== null){
    messageFormResponse = document.getElementById("message-update-teacher");
    var form = document.getElementById("form-update-teacher");
    document.getElementById("submit-update-teacher").addEventListener("click", function(event){
        event.preventDefault();
        var idTeacher = document.getElementById("updated-value");
        var nameTeacher = document.getElementById("name-teacher");
        var firstNmaeTeacher = document.getElementById("first-name-teacher");
        var desTeacher = document.getElementById("des-teacher");
        nameTeacher.style.border = "";
        firstNmaeTeacher.style.border = "";
        desTeacher.style.border = "";
        if(controlFormRegexLength(nameTeacher, /^([a-zA-Z0-9 _,.;]{3,})$/, 3) && 
            controlFormRegexLength(firstNmaeTeacher, /^([a-zA-Z0-9 _,.;]{3,})$/, 3) &&
            controlFormRegexLength(desTeacher, /^([a-zA-Z0-9 _,.;]{3,})$/, 3)){
            var formDatapost = new FormData();
            formDatapost.append('id-teacher', JSON.stringify(idTeacher.value));
            formDatapost.append('name-teacher', JSON.stringify(nameTeacher.value));
            formDatapost.append('first-name-teacher', JSON.stringify(firstNmaeTeacher.value));
            formDatapost.append('des-teacher', JSON.stringify(desTeacher.value));

            ajaxPost("../api-php/api/teachers/update_teacher.php", formDatapost, callback_succes, callback_fail, false);
        }
    });
}

/**
 *  request ajax for delete teachers
 */
function delete_teacher(id, elmt){
    messageFormResponse = document.getElementById("message-show-teachers");
    
    ajaxPost("../api-php/api/teachers/delete_teacher.php?id=" + parseInt(id), null, 
    function callback_succes(reqMessage){
        messageFormResponse.style.border = "solid 2px green";
        messageFormResponse.style.display = "";
        messageFormResponse.style.backgroundColor = "darkgrey";
        messageFormResponse.style.color = "green";
        elmt.parentElement.remove();
        var reqObjt = JSON.parse(reqMessage);
        messageFormResponse.innerHTML = reqObjt.message;
        setTimeout(function(){
            messageFormResponse.innerHTML = "";
            messageFormResponse.style.display = "none";
            location.reload();
        }, 2000);
    }, function callback_fail(reqMessage){
        messageFormResponse.style.border = "solid 2px red";
        messageFormResponse.style.display = "";
        messageFormResponse.style.backgroundColor = "darkgrey";
        messageFormResponse.style.color = "red";
        var reqObjt = JSON.parse(reqMessage);
        messageFormResponse.innerHTML = reqObjt.message;
        setTimeout(function(){
            messageFormResponse.innerHTML = "";
            messageFormResponse.style.display = "none";
        }, 2000);
    }, false);
}