/**
 * request ajax for create courses
 * 
 */
var messageFormResponse = null;

if(document.getElementById("message-add-courses") !== null){
    messageFormResponse = document.getElementById("message-add-courses");
    var form = document.getElementById("form-add-courses");
    document.getElementById("submit-course").addEventListener("click", function(event){
        event.preventDefault();
        var nameCourse = document.getElementById("name-course");
        var desCourse = document.getElementById("des-course");
        nameCourse.style.border = "";
        desCourse.style.border = "";
        if(controlFormRegexLength(desCourse, /^([a-zA-Z0-9 _,.;]{3,})$/, 3) && controlFormRegexLength(nameCourse, /^([a-zA-Z0-9 _,.;]{3,})$/, 3)){
            var formDatapost = new FormData();
            formDatapost.append('name-course', JSON.stringify(nameCourse.value));
            formDatapost.append('des-course', JSON.stringify(desCourse.value));

            ajaxPost("../api-php/api/courses/create_course.php", formDatapost, callback_succes, callback_fail, false);
        }
    });
}

/**
 * request ajax for update courses
 * 
 */
if(document.getElementById("message-update-courses") !== null){
    messageFormResponse = document.getElementById("message-update-courses");
    var form = document.getElementById("form-update-courses");
    document.getElementById("submit-update-courses").addEventListener("click", function(event){
        event.preventDefault();
        var idCourse = document.getElementById("updated-value");
        var nameCourse = document.getElementById("name-course");
        var desCourse = document.getElementById("des-course");
        nameCourse.style.border = "";
        desCourse.style.border = "";
        if(controlFormRegexLength(desCourse, /^([a-zA-Z0-9 _,.;]{3,})$/, 3) && controlFormRegexLength(nameCourse, /^([a-zA-Z0-9 _,.;]{3,})$/, 3)){
            var formDatapost = new FormData();
            formDatapost.append('id-course', JSON.stringify(idCourse.value));
            formDatapost.append('name-course', JSON.stringify(nameCourse.value));
            formDatapost.append('des-course', JSON.stringify(desCourse.value));

            ajaxPost("../api-php/api/courses/update_course.php", formDatapost, callback_succes, callback_fail, false);
        }
    });
}

/**
 *  request ajax for delete courses
 */
function delete_course(id, elmt){
    messageFormResponse = document.getElementById("message-show-courses");
    
    ajaxPost("../api-php/api/courses/delete_course.php?id=" + parseInt(id), null, 
    function callback_succes(reqMessage){
        messageFormResponse.style.border = "solid 2px green";
        messageFormResponse.style.display = "";
        messageFormResponse.style.backgroundColor = "darkgrey";
        messageFormResponse.style.color = "green";
        var reqObjt = JSON.parse(reqMessage);
        messageFormResponse.innerHTML = reqObjt.message;
        elmt.parentElement.remove();
        setTimeout(function(){
            messageFormResponse.innerHTML = "";
            messageFormResponse.style.display = "none";
            location.reload();
        }, 2000);
    }, function callback_fail(reqMessage){
        messageFormResponse.style.border = "solid 2px red";
        messageFormResponse.style.display = "";
        messageFormResponse.style.backgroundColor = "darkgrey";
        messageFormResponse.style.color = "red";
        var reqObjt = JSON.parse(reqMessage);
        messageFormResponse.innerHTML = reqObjt.message;
        setTimeout(function(){
            messageFormResponse.innerHTML = "";
            messageFormResponse.style.display = "none";
            location.reload();
        }, 2000);
    }, false);
}