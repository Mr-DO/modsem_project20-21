/**
 * request ajax for get teachers
 * 
 */
var messageFormResponse = null;
var form = null; 

if(document.getElementById("course-reservation") !== null){
    messageFormResponse = document.getElementById("message-add-reservation");
    form = document.getElementById("form-add-reservation");
    document.getElementById("course-reservation").addEventListener("change", function(event){
        if((document.getElementById("course-reservation").value.length) > 0){
            var idCourse = document.getElementById("course-reservation").value;
            var elmtTeacher = document.getElementById("teacher-reservation");
            var listChildNodes = elmtTeacher.querySelectorAll("option");
            ajaxGet("../api-php/api/teachers/get_teachers.php?idCourse=" + idCourse, function(reqMessage){
                var objResponse = JSON.parse(reqMessage);
                var teachers = objResponse.teachers;
                listChildNodes.forEach((node) => {
                        if(node.tagName === 'OPTION' && node.value !== '') elmtTeacher.removeChild(node);
                    }
                );
                teachers.forEach(element => {
                    var elmtOption = document.createElement('OPTION');
                    elmtOption.setAttribute("value", element[0]);
                    elmtOption.appendChild(document.createTextNode(element[1]));
                    elmtTeacher.appendChild(elmtOption);
                });
                setTimeout(function(){
                    messageFormResponse.innerHTML = "";
                    messageFormResponse.style.display = "none";
                }, 2000);
            }, function(reqMessage){
                var messageFormResponse = document.getElementById("message-add-reservation");
                messageFormResponse.style.border = "solid 2px red";
                messageFormResponse.style.display = "";
                messageFormResponse.style.backgroundColor = "darkgrey";
                messageFormResponse.style.color = "red";
                var reqObjt = JSON.parse(reqMessage);
                messageFormResponse.innerHTML = reqObjt.message;
                listChildNodes.forEach((node) => {
                        if(node.tagName === 'OPTION' && node.value !== '') elmtTeacher.removeChild(node);
                    }
                );
                setTimeout(function(){
                    messageFormResponse.innerHTML = "";
                    messageFormResponse.style.display = "none";
                }, 2000);
            });
        }
    });
}

/**
 * request ajax for get hours
 * 
 */
if(document.getElementById("date-reservation") !== null){
    messageFormResponse = document.getElementById("message-add-reservation");
    form = document.getElementById("form-add-reservation");
    document.getElementById("date-reservation").addEventListener("change", function(event){

        var elmtCourse = document.getElementById("course-reservation");
        var idCourseRes = elmtCourse.options[elmtCourse.selectedIndex].value;

        var elmtTeacher= (document.getElementById("teacher-reservation"));
        var idTeacherRes = elmtTeacher.options[elmtTeacher.selectedIndex].value;

        var hourRes = document.getElementById("hour-reservation");
        var dateRes = document.getElementById("date-reservation");
        var listChildNodes = hourRes.querySelectorAll("option");
        if(required_value(elmtCourse) && required_value(elmtTeacher) && required_value(dateRes)){
            ajaxGet("../api-php/api/hours/get_hours.php?idCourse=" + idCourseRes + '&idTeacher=' + idTeacherRes + '&date=' + dateRes.value, function(reqMessage){
                var objResponse = JSON.parse(reqMessage);
                var hours = objResponse.hours;
                listChildNodes.forEach( node => {
                        if(node.tagName === 'OPTION' && node.value !== '') hourRes.removeChild(node);
                    }
                );
                hours.forEach(element => {
                    var elmtOption = document.createElement('OPTION');
                    elmtOption.setAttribute("value", element);
                    elmtOption.appendChild(document.createTextNode(element));
                    hourRes.appendChild(elmtOption);
                });
                setTimeout(function(){
                    messageFormResponse.innerHTML = "";
                    messageFormResponse.style.display = "none";
                }, 2000);
            }, function(reqMessage){
                var messageFormResponse = document.getElementById("message-add-reservation");
                messageFormResponse.style.border = "solid 2px red";
                messageFormResponse.style.display = "";
                messageFormResponse.style.backgroundColor = "darkgrey";
                messageFormResponse.style.color = "red";
                var reqObjt = JSON.parse(reqMessage);
                messageFormResponse.innerHTML = reqObjt.message;
                listChildNodes.forEach((node) => {
                        if(node.tagName === 'OPTION' && node.value !== '') hourRes.removeChild(node);
                    }
                );
                setTimeout(function(){
                    messageFormResponse.innerHTML = "";
                    messageFormResponse.style.display = "none";
                }, 2000);
            });
        }
    });
}


/**
 * request ajax for create reservations
 * 
 */

if(document.getElementById("submit-reservation") !== null){
    messageFormResponse = document.getElementById("message-add-reservation");
    form = document.getElementById("form-add-reservation");
    document.getElementById("submit-reservation").addEventListener("click", function(event){
        event.preventDefault();

        var elmtCourse = document.getElementById("course-reservation");
        var idCourseRes = elmtCourse.options[elmtCourse.selectedIndex].value;

        var elmtTeacher= (document.getElementById("teacher-reservation"));
        var idTeacherRes = elmtTeacher.options[elmtTeacher.selectedIndex].value;

        var hourRes = document.getElementById("hour-reservation");
        var dateRes = document.getElementById("date-reservation");

        if(required_value(elmtCourse) && required_value(elmtTeacher) && required_value(hourRes) && required_value(dateRes)){
            var formDatapost = new FormData();
            formDatapost.append('id-course', JSON.stringify(idCourseRes));
            formDatapost.append('id-teacher', JSON.stringify(idTeacherRes));
            formDatapost.append('date', JSON.stringify(dateRes.value));
            formDatapost.append('hour', JSON.stringify(hourRes.value));

            ajaxPost("../api-php/api/active/create_reservation.php", formDatapost, callback_succes, callback_fail, false);
        }
    });
}

