/**
 * request ajax for register users
 */
messageFormResponse = document.getElementById("message-register-form");

document.getElementById("submit-register").addEventListener("click", function(event){
    event.preventDefault();
    var usernameElmt = document.getElementById("username-register");
    var passwordElmt = document.getElementById("password-register");
    var confirmpasswordElmt = document.getElementById("confirm-password-register");
    usernameElmt.style.border = "";
    passwordElmt.style.border = "";
    confirmpasswordElmt.style.border = "";
    //if(control_form_register(usernameElmt, passwordElmt, confirmpasswordElmt)){
    if(controlFormRegexLength(usernameElmt, /^([a-zA-Z0-9 \-@_.]{3,})$/, 3) &&
        controlFormRegexLength(passwordElmt, /^([a-zA-Z0-9]{6,})$/, 6) &&
        controlFormRegexLength(confirmpasswordElmt, /^([a-zA-Z0-9]{6,})$/, 6) &&
        (passwordElmt.value === confirmpasswordElmt.value)){
        var formDatapost = new FormData();
        formDatapost.append('username', JSON.stringify(usernameElmt.value));
        formDatapost.append('password', JSON.stringify(passwordElmt.value));

        ajaxPost("../api-php/api/users/create_user.php", formDatapost, function callback_succes(reqMessage){
            messageFormResponse.style.border = "solid 2px green";
            messageFormResponse.style.display = "";
            messageFormResponse.style.backgroundColor = "darkgrey";
            messageFormResponse.style.color = "green";
            var reqObjt = JSON.parse(reqMessage);
            messageFormResponse.innerHTML = reqObjt.message + " Puo fare login <a href=" + "login.php?username=" + reqObjt.username + " alt=" + "login page" + "> clica qui.</a>";
            setTimeout(function(){
                messageFormResponse.innerHTML = "";
                messageFormResponse.style.display = "none";
            }, 60000);
        }, callback_fail, false);
    }
    else{
        if(passwordElmt.value !== confirmpasswordElmt.value){
            passwordElmt.style.border = "solid 2px red";
            confirmpasswordElmt.style.border = "solid 2px red";
        }
    }
});
