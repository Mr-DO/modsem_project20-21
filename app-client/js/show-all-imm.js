function alertNoResulta(idElmnt, lengthResult)
{
    var elmnt = document.getElementById(idElmnt);
    elmnt.innerHTML = lengthResult + " Risultati ottenuti.";
}

function hideElement(id_element)
{
    $(id_element).hide();
}

function showElement(id_element)
{
    $(id_element).show();
}
//haNazioneDiOrigine o haNazioneDiResidenza
function makeQuery(name, lastName, nazioneO, nazioneR, continenteO="", continenteR="", anno, motivazione)
{

    console.log("nome : " + name + "\n");
    console.log("cognome : " + lastName + "\n");
    console.log("nazioneO : " + nazioneO + "\n");
    console.log("nazioneR : " + nazioneR + "\n");
    console.log("anno : " + anno + "\n");
    console.log("continenteO : " + continenteO + "\n");
    console.log("continenteR : " + continenteR + "\n");
    console.log("continenteR : " + motivazione + "\n");

    var name_ =  (name == "" ? "" : " FILTER(?nome" + " = \"" + name + "\"^^xsd:string). ");
    var lastName_ = (lastName == "" ? "" : " FILTER(?cognome" + " = \"" + lastName + "\"^^xsd:string). ");
    var anno_ = (anno == "" ? "" : " FILTER(?anno" + " = \"" + anno + "\"^^xsd:decimal). ");
    var motivazione_ = (motivazione == "" ? "" : " FILTER(?motivazione" + " = imm:" + motivazione + "). ");
    var nazioneO_ = (nazioneO == "" ? "?nazioneO" : "imm:" + nazioneO);
    var nazioneR_ =(nazioneR == "" ? "?nazioneR" : "imm:" + nazioneR)
    var continenteO_ = (continenteO == "" ? "?continenteO" : "imm:" + continenteO);
    var continenteR_ = (continenteR == "" ? "?continenteR" : "imm:" + continenteR);
    return "SELECT DISTINCT ?nome ?cognome ?nazioneO ?nazioneR ?continenteO ?continenteR ?anno ?motivazione "
        +"WHERE {"
            + "  ?subject rdf:type ?Immigrato"
            + "; imm:haNazioneDiOrigine " + nazioneO_
            + "; imm:haNazioneDiResidenza " + nazioneR_
            + "; imm:haNome ?nome"
            + "; imm:haCognome ?cognome"
            + ". " + nazioneO_ + " imm:situatoInContinente " + continenteO_
            + ". " + nazioneR_ + " imm:situatoInContinente " + continenteR_
            + ". ?subject imm:haImmigratoPer ?motivazione"
            + ". OPTIONAL { ?subject imm:haNome ?nome.}"
            + ". OPTIONAL { ?subject imm:haCognome ?cognome.}"
            + ". OPTIONAL { ?subject imm:haImmigratoInAnno ?anno.}." 
            + name_ + lastName_ + anno_ + motivazione_ +
        "}";
}

//haNazioneDiOrigine o haNazioneDiResidenza
function makeQueryInsert(name, lastName, nazioneO, nazioneR, anno, motivazione)
{

    console.log("nome : " + name + "\n");
    console.log("cognome : " + lastName + "\n");
    console.log("nazioneO : " + nazioneO + "\n");
    console.log("nazioneR : " + nazioneR + "\n");
    console.log("anno : " + anno + "\n");
    console.log("motivazione : " + motivazione + "\n");

    var individual = "imm:" + name + lastName;
    var individual_insert =  (name == "" ? "" : individual + " rdf:type owl:NamedIndividual" + ". ");
    var name_ = (name == "" ? "" : individual + " imm:haNome \"" + name + "\"^^xsd:string. ");
    var lastName_ = (lastName == "" ? "" : individual + " imm:haCognome \"" + lastName + "\"^^xsd:string. ");
    var anno_ = (anno == "" ? "" : individual + " imm:haImmigratoInAnno \"" + anno +"\"^^xsd:decimal. ");
    var motivazione_ = (motivazione == "" ? "" : individual + " imm:haImmigratoPer imm:" + motivazione + ". ");
    var nazioneO_ = (nazioneO == "" ? "" : individual + " imm:haNazioneDiOrigine imm:" + nazioneO + ". ");
    var nazioneR_ =(nazioneR == "" ? "" : individual + " imm:haNazioneDiResidenza imm:" + nazioneR + ". ");
    return "PREFIX owl: <http://www.w3.org/2002/07/owl#> \n"
        +  "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> \n"
        +  "PREFIX imm: <http://www.semanticweb.org/ousmane/modsem_project2021#> \n"
        +  "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#> \n"
        +  "INSERT DATA {" + "\n"
        +  "GRAPH <http://www.semanticweb.org/ousmane/modsemproject20-21> { \n"
        +       individual_insert + "\n"
        +       name_ + "\n" 
        +       lastName_ + "\n"
        +       nazioneO_ + "\n"
        +       nazioneR_ + "\n"
        +       anno_ + "\n"
        +       motivazione_ + "\n"
        +   "}" + "\n"
        +  "}";
}

function transforIfUri(item)
{
    if(item.type == "uri")
    {
        return "<a href=\"" + item.value + "\">" + item.value + "</a>";
    }
    return item.value;
}

function createUlNone(keysItems, listItems, codeColor)
{
    var ulNode = (codeColor%2==0)?": rgb(202, 209, 226); border: 2px solid red; ":": rgb(255,250,191); border: 2px solid blue;";
    ulNode = "<ul style=\"background-color" + ulNode + "border-radius: 25px; \">" 
    for(var i=0; i<listItems.length; i++)
    {
        ulNode += "<li><p>" + keysItems[i]+ " : " + transforIfUri(listItems[i]) +"</p></li>"
    }
    ulNode += "</ul>"
    return ulNode;
}

function addResults(idElmnt, results)
{
    var elmnt = document.getElementById(idElmnt);
    var divNode = "";
    for(var i=0; i < results.length; i++)
    {
        divNode += createUlNone(Object.keys(results[i]), Object.values(results[i]), i);
    }
    elmnt.innerHTML = divNode;
}