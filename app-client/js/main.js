function hamburgerGestion(elem){
    if(elem.id === "hamburger_open"){
        var open = document.getElementById("elements-left-menu");
        var hamburger = document.getElementById("hamburger_close");
        hamburger.style.display = "block";
        open.style.display = "none";
        document.getElementById("hamburger_open").style.display = "none";
        document.getElementsByClassName("left-menu")[0].style.width = "5%";
        document.getElementsByClassName("div-sections")[0].style.width = "75%";
    }
    else if(elem.id === "hamburger_close"){
        var close = document.getElementById("elements-left-menu");
        var hamburger = document.getElementById("hamburger_open");
        hamburger.style.display = "block";
        close.style.display = "";
        document.getElementById("hamburger_close").style.display = "none";
        console.log(screen.width);
        console.log(window.innerWidth);
        if(window.innerWidth <= 900) document.getElementsByClassName("left-menu")[0].style.width = "100%";
        else document.getElementsByClassName("left-menu")[0].style.width = "15%";
        document.getElementsByClassName("div-sections")[0].style.width = "60%";
    }
}