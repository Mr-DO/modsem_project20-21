if(document.getElementById("contact-footer") !== null) document.getElementById("contact-footer").style.display = "none";
if(document.getElementById("areariservata-footer") !== null) document.getElementById("areariservata-footer").style.display = "none";


function callback_succes(reqMessage){
    messageFormResponse.style.border = "solid 2px green";
    messageFormResponse.style.display = "";
    messageFormResponse.style.backgroundColor = "darkgrey";
    messageFormResponse.style.color = "green";
    console.log(reqMessage);
    var reqObjt = JSON.parse(reqMessage);
    messageFormResponse.innerHTML = reqObjt.message;
    form.reset();
    setTimeout(function(){
        messageFormResponse.innerHTML = "";
        messageFormResponse.style.display = "none";
        location.reload();
    }, 2000);
}

function callback_fail(reqMessage){
    messageFormResponse.style.border = "solid 2px red";
    messageFormResponse.style.display = "";
    messageFormResponse.style.backgroundColor = "darkgrey";
    messageFormResponse.style.color = "red";
    var reqObjt = JSON.parse(reqMessage);
    messageFormResponse.innerHTML = reqObjt.message;
    setTimeout(function(){
        messageFormResponse.innerHTML = "";
        messageFormResponse.style.display = "none";
        location.reload();
    }, 2000);
}

function controlFormRegexLength(elmtInput, regExp, length){
    if(!(regExp.test(elmtInput.value.trim()) && (elmtInput.value.length >= length))){
        elmtInput.style.border = "solid 2px red";
        return false;
    }
    return true;
}

function required_value(elmt){
    if(elmt.value === ''){
        elmt.style.border = 'solid 2px red';
        return false;
    }
    elmt.style.border = '';
    return true;
}
