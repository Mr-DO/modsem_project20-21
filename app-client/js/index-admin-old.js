document.getElementById("contact-footer").style.display = "none";
document.getElementById("areariservata-footer").style.display = "none";

var messageFormCreateCourses = null;

var list = [];
var pageList = [];
var currentPage = 1;
var numberPerPage = 2;
var numberOfPages = 0;

/**
 *  Fare la paginazione
 */
makeList();
if(document.getElementById("next") !== null) {
    check();
    document.getElementById("next").addEventListener("click", function(){ numberOfPages = getNumberOfPages(); load(); });
    document.getElementById("previous").addEventListener("click", function(){ numberOfPages = getNumberOfPages(); load(); });
    document.getElementById("first").addEventListener("click", function(){ numberOfPages = getNumberOfPages(); load(); });
    document.getElementById("last").addEventListener("click", function(){ numberOfPages = getNumberOfPages(); load(); });
}

/**
 * request ajax for create courses
 * 
 */
if(document.getElementById("submit-course") !== null){
    messageFormCreateCourses = document.getElementById("message-add-courses");
    document.getElementById("submit-course").addEventListener("click", function(event){
        event.preventDefault();
        var nameCourse = document.getElementById("name-course");
        var desCourse = document.getElementById("des-course");
        nameCourse.style.border = "";
        desCourse.style.border = "";
        if(control_form_add_course(nameCourse, desCourse)){
            var formDatapost = new FormData();
            formDatapost.append('name-course', JSON.stringify(nameCourse.value));
            formDatapost.append('des-course', JSON.stringify(desCourse.value));

            ajaxPost("../api-php/api/courses/create_course.php", formDatapost, callback_succes, callback_fail, false);
        }
        else{
            control_form_add_course_feedback(nameCourse, desCourse);
        }
    });
}

/**
 *  request ajax for delete courses
 */
function delete_course(id, elmt){
    messageFormCreateCourses = document.getElementById("message-show-courses");
    
    ajaxPost("../api-php/api/courses/delete_course.php?id=" + parseInt(id), null, 
    function callback_succes(reqMessage){
        messageFormCreateCourses.style.border = "solid 2px green";
        messageFormCreateCourses.style.display = "";
        messageFormCreateCourses.style.backgroundColor = "darkgrey";
        messageFormCreateCourses.style.color = "green";
        var reqObjt = JSON.parse(reqMessage);
        messageFormCreateCourses.innerHTML = reqObjt.message;
        elmt.parentElement.remove();
    }, function callback_fail(reqMessage){
        messageFormCreateCourses.style.border = "solid 2px red";
        messageFormCreateCourses.style.display = "";
        messageFormCreateCourses.style.backgroundColor = "darkgrey";
        messageFormCreateCourses.style.color = "red";
        var reqObjt = JSON.parse(reqMessage);
        messageFormCreateCourses.innerHTML = reqObjt.message;
    }, false);
}

/**
 * request ajax for create Teachers
 * 
 */
if(document.getElementById("submit-teacher") !== null){
    messageFormCreateCourses = document.getElementById("message-add-teacher");
    document.getElementById("submit-teacher").addEventListener("click", function(event){
        event.preventDefault();

        var nameTeacher = document.getElementById("name-teacher");
        var firstnameTeacher = document.getElementById("first-name-teacher");
        var courseTeacher = document.getElementById("course-teacher");
        var desTeacher = document.getElementById("des-teacher");

        nameTeacher.style.border = "";
        firstnameTeacher.style.border = "";
        courseTeacher.style.border = "";
        desTeacher.style.border = "";

        var pass_control = control_form_add(nameTeacher, /^([a-zA-Z0-9 -_]{2,})$/) && control_form_add(firstnameTeacher, /^([a-zA-Z0-9 -_]{2,})$/) && (parseInt(courseTeacher.value) > 0) && control_form_add_length(desTeacher, 10);
        if(pass_control){
            var formDatapost = new FormData();
            formDatapost.append('name-teacher', JSON.stringify(nameTeacher.value));
            formDatapost.append('first-name-teacher', JSON.stringify(firstnameTeacher.value));
            formDatapost.append('course-teacher', JSON.stringify(courseTeacher.value));
            formDatapost.append('des-teacher', JSON.stringify(desTeacher.value));

            ajaxPost("../api-php/api/teachers/create_teacher.php", formDatapost, callback_succes, callback_fail, false);
        }
        else{
            control_form_add_feedback(nameTeacher, /^([a-zA-Z0-9 -_]{2,})$/);
            control_form_add_feedback(firstnameTeacher, /^([a-zA-Z0-9 -_]{2,})$/);
            control_form_add_feedback(courseTeacher, /^([0-9]{1,})$/)
            control_form_add_length_feedback(desTeacher, 10);
        }
    });
}

/**
 *  request ajax for delete teachers
 */
function delete_teacher(id, elmt){
    messageFormCreateCourses = document.getElementById("message-show-teachers");
    
    ajaxPost("../api-php/api/teachers/delete_teacher.php?id=" + parseInt(id), null, 
    function callback_succes(reqMessage){
        messageFormCreateCourses.style.border = "solid 2px green";
        messageFormCreateCourses.style.display = "";
        messageFormCreateCourses.style.backgroundColor = "darkgrey";
        messageFormCreateCourses.style.color = "green";
        var reqObjt = JSON.parse(reqMessage);
        messageFormCreateCourses.innerHTML = reqObjt.message;
        elmt.parentElement.remove();
        makeList();
        if(document.getElementById("next") !== null) {
            check();
            document.getElementById("next").addEventListener("click", function(){ numberOfPages = getNumberOfPages(); load(); });
            document.getElementById("previous").addEventListener("click", function(){ numberOfPages = getNumberOfPages(); load(); });
            document.getElementById("first").addEventListener("click", function(){ numberOfPages = getNumberOfPages(); load(); });
            document.getElementById("last").addEventListener("click", function(){ numberOfPages = getNumberOfPages(); load(); });
        }
    }, callback_fail, false);
}

function showItem(classItem, classInfo, tittle, description, id){
    var nodeDiv = document.createElement("DIV");
    nodeDiv.setAttribute("class", classItem);
    nodeDiv.style.justifyContent = "center";

    var nodeDivIntern = document.createElement("DIV");
    nodeDivIntern.setAttribute("class", classInfo);
    nodeDivIntern.style.backgroundColor = "rgb(148, 163, 158)";

    var nodeTittle = document.createElement("H4");
    nodeTittle.style.marginBottom = "0px";
    nodeDivIntern.style.color = "red";
    nodeTittle.appendChild(document.createTextNode(tittle));

    var nodeDescription = document.createElement("P");
    nodeDescription.style.marginTop = "0px";
    nodeDescription.style.color = "black";
    nodeDescription.appendChild(document.createTextNode(description));

    var nodeButtonDelete  = document.createElement("INPUT");
    nodeButtonDelete.setAttribute("type", "button");
    nodeButtonDelete.style.backgroundColor = "red";
    nodeButtonDelete.style.marginRight = "5px";
    nodeButtonDelete.onclick = function() { delete_course(id, this) };
    nodeButtonDelete.value = "DELETE";

    var nodeButtonUpdate  = document.createElement("INPUT");
    nodeButtonUpdate.setAttribute("type", "button");
    nodeButtonUpdate.style.backgroundColor = "green";
    nodeButtonUpdate.style.marginRight = "5px";
    nodeButtonUpdate.value = "UPDATE";

    nodeDivIntern.appendChild(nodeTittle);
    nodeDivIntern.appendChild(nodeDescription);

    nodeDiv.appendChild(nodeDivIntern);
    nodeDiv.appendChild(nodeButtonDelete);
    nodeDiv.appendChild(nodeButtonUpdate);
    nodeDiv.appendChild(document.createElement("HR"));

    return nodeDiv;
}


function makeList() {
    if(document.getElementById('show-courses') !== null){
        ajaxGet("../api-php/api/courses/get_courses.php", function callback_succes(req){
            var reqIn = JSON.parse(req);
            list = [];
            for (x=0; x < reqIn.data.length; x++)
                list.push(reqIn.data[x]);
        }, function callback_fail(req){
            var reqIn = JSON.parse(req);
            console.log(reqIn.data);
        });

    }else if(document.getElementById('show-reservations') !== null){

    }
    else if(document.getElementById('show-teachers') !== null){
        ajaxGet("../api-php/api/teachers/get_teachers.php", function callback_succes(req){
            var reqIn = JSON.parse(req);
            console.log(reqIn.data);
            list = [];
            for (x=0; x < reqIn.data.length; x++)
                list.push(reqIn.data[x]);
        }, function callback_fail(req){
            var reqIn = JSON.parse(req);
            console.log(reqIn);
        });
    }
    numberOfPages = getNumberOfPages();
}
    
function getNumberOfPages() {
    return Math.ceil(list.length / numberPerPage);
}

function nextPage() {
    currentPage += 1;
    loadList();
}

function previousPage() {
    currentPage -= 1;
    loadList();
}

function firstPage() {
    currentPage = 1;
    loadList();
}

function lastPage() {
    currentPage = getNumberOfPages();
    loadList();
}

function loadList() {
    var begin = ((currentPage - 1) * numberPerPage);
    var end = begin + numberPerPage;
    pageList = list.slice(begin, end);
    console.log("begin : " + begin + " end : " + end + " current page : " + currentPage + " number of page : " + numberOfPages);
    drawList();
    check();
}
    
function drawList() {
    var elmtView;
    if(document.getElementById('show-courses') !== null){
        elmtView = document.getElementById("content-show-courses");
    }else if(document.getElementById('show-reservations') !== null){
        elmtView = document.getElementById("content-show-reservations");
    }else if(document.getElementById('show-teachers') !== null){ 
        elmtView = document.getElementById("content-show-teachers");
    }
    elmtView.innerHTML = "";
    for (r = 0; r < pageList.length; r++) {
        elmtView.appendChild(showItem("item-show", "item-info", pageList[r].name, pageList[r].description, pageList[r].id));
    }
}

function check() {
    document.getElementById("next").disabled = currentPage == numberOfPages ? true : false;
    document.getElementById("previous").disabled = currentPage == 1 ? true : false;
    document.getElementById("first").disabled = currentPage == 1 ? true : false;
    document.getElementById("last").disabled = currentPage == numberOfPages ? true : false;
}

function load() {
    loadList();
}


function callback_succes(reqMessage){
    messageFormCreateCourses.style.border = "solid 2px green";
    messageFormCreateCourses.style.display = "";
    messageFormCreateCourses.style.backgroundColor = "darkgrey";
    messageFormCreateCourses.style.color = "green";
    console.log(reqMessage);
    var reqObjt = JSON.parse(reqMessage);
    messageFormCreateCourses.innerHTML = reqObjt.message;
}

function callback_fail(reqMessage){
    messageFormCreateCourses.style.border = "solid 2px red";
    messageFormCreateCourses.style.display = "";
    messageFormCreateCourses.style.backgroundColor = "darkgrey";
    messageFormCreateCourses.style.color = "red";
    var reqObjt = JSON.parse(reqMessage);
    messageFormCreateCourses.innerHTML = reqObjt.message;
}

function control_form_add_course(nameCourse, desCourse){
    if(!(/^([a-zA-Z0-9 -_]{2,})$/.test(nameCourse.value.trim()))) return false;
    if(!((desCourse.value.trim()).length >= 10)) return false;
    return true; 
}

function control_form_add_course_feedback(nameCourse, desCourse){
    if(!(/^([a-zA-Z0-9 ]{2,})$/.test(nameCourse.value.trim()))){
        nameCourse.style.border = "solid 2px red";
    }else{
        nameCourse.style.border = ""; 
    }
    if(!((desCourse.value.trim()).length >= 10)){
        desCourse.style.border = "solid 2px red";
    }else{
        desCourse.style.border = "";
    }
}

function control_form_add(elmt, regExp){
    if(!(regExp.test(elmt.value.trim()))) return false;
    return true; 
}

function control_form_add_length(elmt, length){
    if(!((elmt.value.trim().length >= length))) return false;
    return true; 
}

function control_form_add_feedback(elmt, regExp){
    if(!control_form_add(elmt, regExp)){
        elmt.style.border = "solid 2px red";
    }else{
        elmt.style.border = ""; 
    }
}

function control_form_add_length_feedback(elmt, length){
    if(!control_form_add_length(elmt, length)){
        elmt.style.border = "solid 2px red";
    }else{
        elmt.style.border = ""; 
    }
}